import { Box } from '@mui/material';
import React, { createContext, useEffect } from 'react';
import { useState } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { AuthContext } from './Context/Context';
import CreateTeam from './views/ViewsFromSideBar/CreateTeam/CreateTeam';
import Dashboard from './views/Dashboard/Dashboard';
import Home from './views/Home/Home';
import Assignments from './views/ViewsFromSideBar/Assignments/Assignments';
import MyResearch from './views/ViewsFromSideBar/MyResearch/MyResearch';
import Notifications from './views/ViewsFromSideBar/Notifications/Notifications';
import AdminPanel from './views/AdminView/AdminPanel';

import Profile from './views/ViewsFromSideBar/Profile/Profile';
import SingleResearchPreview from './views/SingleResearchPreview/SingleResearchPreview';
import TeamView from './views/TeamView.tsx/TeamView';
import NotFound from './views/404/404';
import { DragDropContext } from 'react-beautiful-dnd';
import { changeItemStatus, getResearchById } from './services/researchItem';
import { pushNotify } from './notify/notify';
import { idText } from 'typescript';

const App: React.FC = () => {
  const [dragAndDrop, setDragAndDrop] = useState({
    itemId: '',
    newStatus: '',
    destination: '',
    source: '',
  });
  const [appState, setAppState] = useState({
    user: null,
    userData: null,
  });

  const handleDrag = (result: any) => {
    if (result.destination === null) return;

    const itemId = result.draggableId;
    const newStatus = result.destination.droppableId;
    const destination = result.destination;
    const source = result.source;
    const userData: any = appState.userData;

    getResearchById(itemId).then((item) => {
      if (
        item.val().assignee === userData?.email ||
        item.val().creator === userData?.uid
      ) {
        changeItemStatus(itemId, newStatus).then(() =>
          setDragAndDrop({ itemId, newStatus, destination, source })
        );
      } else {
        return pushNotify('error', 'Error', "You don't have this privilege");
      }
    });
  };

  return (
    <BrowserRouter>
      <DragDropContext onDragEnd={handleDrag}>
        <AuthContext.Provider
          value={{
            ...appState,
            setContext: setAppState,
          }}
        >
          <Box className='App'>
            <Routes>
              <Route path='*' element={<NotFound />} />
              <Route path='/' element={<Dashboard />}>
                <Route path='create-team' element={<CreateTeam />} />
                <Route
                  path='team/:id'
                  element={<TeamView dragAndDrop={dragAndDrop} />}
                />
                <Route
                  path='assignments'
                  element={<Assignments dragAndDrop={dragAndDrop} />}
                />
                <Route
                  path='my-research'
                  element={<MyResearch dragAndDrop={dragAndDrop} />}
                />
                <Route path='notifications' element={<Notifications />} />
                <Route path='profile' element={<Profile />} />
                <Route path='profile/admin-panel' element={<AdminPanel />} />
              </Route>
              <Route
                path='research-item/:id'
                element={<SingleResearchPreview />}
              />
            </Routes>
          </Box>
        </AuthContext.Provider>
      </DragDropContext>
    </BrowserRouter>
  );
};

export default App;
