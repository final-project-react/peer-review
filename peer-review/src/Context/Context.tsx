import { User } from '@firebase/auth';
import { Dispatch, SetStateAction, Context } from 'react';
import { createContext } from 'react';

export interface UserData {
  avatar?: string;
  createdOn?: string;
  email?: string;
  firstName?: string;
  lastName?: string;
  phoneNumber?: string;
  uid?: string;
  userRole?: string;
  username?: string;
  badges: string;
}

interface UserContext {
  user: User | null;
  userData: UserData | null;
  setContext: Dispatch<SetStateAction<{ user: any; userData: any }>>;
}

export const AuthContext: Context<UserContext> = createContext<UserContext>({
  user: null,
  userData: null,
  setContext: () => {},
});

// let newUser = (user as any).asdasdasdasdasdadasd;
