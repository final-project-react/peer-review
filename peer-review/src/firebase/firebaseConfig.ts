import { initializeApp } from 'firebase/app';
import { getAuth } from '@firebase/auth';
import { getDatabase } from '@firebase/database';
import { getStorage } from '@firebase/storage';

const firebaseConfig = {
    apiKey: 'AIzaSyCgHQQTrBPGRQPKbvnzIjUFV_r9H5QOhIY',
    authDomain: 'peer-to-view.firebaseapp.com' ,
    databaseURL: 'https://peer-to-view-default-rtdb.europe-west1.firebasedatabase.app',
    projectId: 'peer-to-view',
    storageBucket: 'gs://peer-to-view.appspot.com' ,
    messagingSenderId: '601755637388',
    appId: '1:601755637388:web:7073ae008b3c68e10bcda9',
    measurementId: 'G-VD81BSS7WD',
};

// Initialize Firebase
export const app = initializeApp({ ...firebaseConfig });

export const auth = getAuth(app);

export const db = getDatabase(app);

export const storage = getStorage(app);
