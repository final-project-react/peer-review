import React from 'react';
import { Box, Card, Grid } from '@mui/material';
import { CardHeader } from '@mui/material';
import ResearchCard from '../../ResearchCard/ResearchCard';
import { gridSpacing, mainColorForMainComponents, mainFont } from '../../../ui-helpers/ui-helpers';
import { NavLink } from 'react-router-dom';
import { ResearchItem } from '../../Interfaces/Interfaces';
import { Draggable, Droppable } from 'react-beautiful-dnd';
import { uid } from 'uid';

const cardHeaderStyle = {
  fontFamily: mainFont,
  color: '#D26868',
  backgroundColor: mainColorForMainComponents,
  borderBottom: '2px solid #B9B9B9',
  position: 'sticky',
  top: '0',
  zIndex: '999',
};

const cardStyle = {
  borderRadius: '13px',
  position: 'relative',
  height: '100%',
  overflow: 'auto',
  backgroundColor: mainColorForMainComponents,
};

interface Props {
  rejected: ResearchItem[] | [];
}


const Rejected = ({ rejected }: Props) => {
  return (
    <Grid item xs={gridSpacing} sx={{ height: '80vh' }}>
      <Card sx={cardStyle}>
        <CardHeader title='Rejected' sx={cardHeaderStyle} />
            <Droppable droppableId='Rejected' >
              {(provided, snapshot) => (
                
              <Grid container spacing={gridSpacing} {...provided.droppableProps} ref={provided.innerRef}>
                  { rejected.length !== 0 ? rejected.map((post: ResearchItem, i: number) => {
                    return (
                      <Draggable 
                        key={post.id}
                        draggableId={(post.id) ?  post.id.toString() : uid().toString()} 
                        index={i}
                        >

                        {(provided, snapshot) => (

                          <Grid key={post.id} item xs={12} ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                            <NavLink to={`/research-item/${post.id}`} className='nav-link'>
                              <ResearchCard post={post} />
                            </NavLink>
                          </Grid>
                        )}

                      </Draggable>
                    );
                  }) 
                  :
                  <Draggable
                    key={rejected.length} 
                    draggableId={rejected.length.toString()}
                    index={rejected.length}
                  >
                    {(provided, snapshot) => 
                    <Grid key={rejected.length} item xs={12} ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                      <Box sx={{color: 'white'}}>_</Box></Grid>}
                  </Draggable>
                }
                  {provided.placeholder}
              </Grid>
              )}
            </Droppable>
      </Card>
    </Grid>
  );
};

export default Rejected;
