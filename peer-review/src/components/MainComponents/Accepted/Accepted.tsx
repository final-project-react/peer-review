import React from 'react';
import { Card, Grid } from '@mui/material';
import { CardHeader } from '@mui/material';
import ResearchCard from '../../ResearchCard/ResearchCard';
import {
  gridSpacing,
  mainColorForMainComponents,
  mainFont,
} from '../../../ui-helpers/ui-helpers';
import { NavLink } from 'react-router-dom';
import { ResearchItem } from '../../Interfaces/Interfaces';
import {
  Draggable,
  Droppable,
} from 'react-beautiful-dnd';
import { uid } from 'uid';
import { Box } from '@mui/system';

const cardHeaderStyle = {
  fontFamily: mainFont,
  color: '#89C043',
  backgroundColor: mainColorForMainComponents,
  borderBottom: '2px solid #B9B9B9',
  position: 'sticky',
  top: '0',
  zIndex: '999',
};

const cardStyle = {
  borderRadius: '13px',
  position: 'relative',
  height: '100%',
  overflow: 'auto',
  backgroundColor: mainColorForMainComponents,
};

interface Props {
  accepted: ResearchItem[] | [];
}

const Accepted = ({ accepted }: any) => {

  return (
    <Grid item xs={gridSpacing} sx={{ height: '80vh' }}>
      <Card sx={cardStyle}>
        <CardHeader title='Accepted' sx={cardHeaderStyle} />

        <Droppable droppableId='Accepted'>
          {(provided, snapshot) => (
            <Grid
              container
              spacing={gridSpacing}
              {...provided.droppableProps}
              ref={provided.innerRef}
            >
              {accepted.length !== 0 ? (
                accepted.map((post: ResearchItem, i: number) => {
                  return (
                    <Draggable
                      key={post.id}
                      draggableId={
                        post.id ? post.id.toString() : uid().toString()
                      }
                      index={i}
                    >
                      {(provided, snapshot) => (
                        <Grid
                          key={post.id}
                          item
                          xs={12}
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                        >
                          <NavLink
                            to={`/research-item/${post.id}`}
                            className='nav-link'
                          >
                            <ResearchCard post={post} />
                          </NavLink>
                        </Grid>
                      )}
                    </Draggable>
                  );
                })
              ) : (
                <Draggable
                  key={accepted.length}
                  draggableId={accepted.length.toString()}
                  index={accepted.length}
                >
                  {(provided, snapshot) => (
                    <Grid
                      key={accepted.length}
                      item
                      xs={12}
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                    >
                      <Box sx={{ color: 'white' }}>_</Box>
                    </Grid>
                  )}
                </Draggable>
              )}
              {provided.placeholder}
            </Grid>
          )}
        </Droppable>
      </Card>
    </Grid>
  );
};

export default Accepted;
