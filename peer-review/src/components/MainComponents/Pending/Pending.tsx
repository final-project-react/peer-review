import React from 'react';
import { Box, Card, Grid } from '@mui/material';
import { CardHeader } from '@mui/material';
import ResearchCard from '../../ResearchCard/ResearchCard';
import {
  gridSpacing,
  mainColorForMainComponents,
  mainFont,
} from '../../../ui-helpers/ui-helpers';
import { NavLink } from 'react-router-dom';
import { ResearchItem } from '../../Interfaces/Interfaces';
import { Draggable, Droppable } from 'react-beautiful-dnd';
import { uid } from 'uid';

const cardHeaderStyle = {
  fontFamily: mainFont,
  color: '#B263CD',
  backgroundColor: mainColorForMainComponents,
  borderBottom: '2px solid #B9B9B9',
  position: 'sticky',
  top: '0',
  zIndex: '999',
};

const cardStyle = {
  borderRadius: '13px',
  position: 'relative',
  height: '100%',
  overflow: 'auto',
  backgroundColor: mainColorForMainComponents,
};

interface Props {
  pending: ResearchItem[] | [];
}

const Pending = ({ pending }: Props) => {

  return (
    <Grid item xs={gridSpacing} sx={{ height: '80vh' }}>
      <Card sx={cardStyle}>
        <CardHeader title='Pending' sx={cardHeaderStyle} />
        <Droppable droppableId='Pending'>
          {(provided, snapshot) => (
            <Grid
              container
              spacing={gridSpacing}
              {...provided.droppableProps}
              ref={provided.innerRef}
            >
              {pending.length !== 0 ? (
                pending.map((post: ResearchItem, i: number) => {
                  return (
                    <Draggable
                      key={post.id}
                      draggableId={
                        post.id ? post.id.toString() : uid().toString()
                      }
                      index={i}
                    >
                      {(provided, snapshot) => (
                        <Grid
                          key={post.id}
                          item
                          xs={12}
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                        >
                          <NavLink
                            to={`/research-item/${post.id}`}
                            className='nav-link'
                          >
                            <ResearchCard post={post} />
                          </NavLink>
                        </Grid>
                      )}
                    </Draggable>
                  );
                })
              ) : (
                <Draggable
                  key={pending.length}
                  draggableId={pending.length.toString()}
                  index={pending.length}
                >
                  {(provided, snapshot) => (
                    <Grid
                      key={pending.length}
                      item
                      xs={12}
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                    >
                      <Box sx={{ color: 'white' }}>_</Box>
                    </Grid>
                  )}
                </Draggable>
              )}
              {provided.placeholder}
            </Grid>
          )}
        </Droppable>
      </Card>
    </Grid>
  );
};

export default Pending;
