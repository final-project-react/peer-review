export const drawerContentStyle = {
  display: 'flex',
  justifyContent: 'space-between',
  height: '42px',
  width: 'auto',
  backgroundColor: 'transparent',
  margin: '14px 14px',
  padding: '12px 0px',
  borderBottom: '1px solid lightgray',
  alignItems: 'flex-end',
};

const open = ''; // shte izmislq i po dobur nachin ...
export const drawerContentOpenClose = {
  flexShrink: 0,
  display: open ? 'none' : { xs: 'none', sm: 'initial' },
  marginBottom: '9px',
};

export const drawerContentTextTitle = {
  display: { xs: 'none', sm: 'initial' },
  fontSize: '18px',
  fontWeight: 600,
  color: 'lightgray',
  width: '154px',
  marginLeft: open ? '0px' : '8px',
  paddingBottom: '3px',
};

export const drawerContentButtonOpenClose = {
  minWidth: 'initial',
  padding: '10px',
  color: 'gray',
  borderRadius: '8px',
  backgroundColor: open ? 'transparent' : 'transparent',
  '&:hover': {
    backgroundColor: '#26284687',
  },
};

export const drawerContentBottomStyle = {
  display: 'flex',
  justifyContent: 'flex-start',
  alignItems: 'center',
  alignContents: 'center',
  margin: '14px 14px',
  padding: '12px 4px',
  borderTop: '1px solid lightgray',
};

export const drawerContentBottomStyleInnerBox = {
  display: 'flex',
  marginRight: '18px',
  paddingLeft: '0px',
  alignItems: 'center',
  alignContent: 'center',
};

export const drawerContentBottomText = {
  display: 'block',
  whiteSpace: 'nowrap',
  lineHeight: 'inherit',
  color: 'lightgray',
};
export const drawerContentBottomName = {
  fontFamily: 'inherit',
  display: 'block',
  whiteSpace: 'nowrap',
  lineHeight: 'inherit',
  fontWeight: 500,
  color: 'lightgray',
};

export const listItemButton = {
  margin: '6px 14px',
  padding: '10px',
  borderRadius: '8px',
  '&:focus': { background: '#26284687' },
};

export const listItemText = {
  display: 'inline',
  margin: '0px',
  overflowX: 'hidden',
  color: 'lightgray',
  whiteSpace: 'nowrap',
  minWidth: '126px',
};
