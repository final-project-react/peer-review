import GroupAddIcon from '@mui/icons-material/GroupAdd';
import GroupsIcon from '@mui/icons-material/Groups';
import AssignmentIcon from '@mui/icons-material/Assignment';
import AssignmentIndIcon from '@mui/icons-material/AssignmentInd';
import NotificationsActiveIcon from '@mui/icons-material/NotificationsActive';
import PersonIcon from '@mui/icons-material/Person';

const SideBarButtons = [
  {
    id: 1,
    path: 'create-team',
    icon: GroupAddIcon,
    desc: 'Create Team',
    secondDesc: '', // tova vurvi s badge i e za notification
    badge: 0,
    subList: [],
  },
  {
    id: 2,
    path: 'owned-teams',
    icon: GroupsIcon,
    desc: 'My teams',
    secondDesc: '',
    badge: 0,
    subList: [],
  },
  {
    id: 3,
    path: 'all-teams',
    icon: GroupsIcon,
    desc: 'All Teams',
    secondDesc: '',
    badge: 0,
    subList: [],
  },
  {
    id: 4,
    path: 'assignments',
    icon: AssignmentIcon,
    desc: 'Assignments',
    secondDesc: '',
    badge: 0,
    subList: [],
  },
  {
    id: 5,
    path: 'my-research',
    icon: AssignmentIndIcon,
    desc: 'My Research',
    secondDesc: '',
    badge: 0,
    subList: [],
  },

  {
    id: 7,
    path: 'profile',
    icon: PersonIcon,
    desc: 'Profile',
    secondDesc: '',
    badge: 0,
    subList: [],
  },
];

export default SideBarButtons;

// {
//   id: 6,
//   path: 'notifications',
//   icon: NotificationsActiveIcon,
//   desc: 'Notifications',
//   secondDesc: '',
//   badge: 0,
//   subList: [],
// },
