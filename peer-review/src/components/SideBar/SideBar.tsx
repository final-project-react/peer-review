import React, { useContext, useEffect, useRef, useState } from 'react';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import MenuIcon from '@mui/icons-material/Menu';
import { AuthContext } from '../../Context/Context';
import { signOut } from '@firebase/auth';
import { Link, useNavigate } from 'react-router-dom';
import { auth } from '../../firebase/firebaseConfig';
import {
  Avatar,
  Badge,
  Chip,
  Divider,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from '@mui/material';
import NotificationsActiveIcon from '@mui/icons-material/NotificationsActive';
import LogoutIcon from '@mui/icons-material/Logout';
import {
  drawerContentStyle,
  drawerContentTextTitle,
  drawerContentButtonOpenClose,
  drawerContentBottomStyle,
  drawerContentBottomStyleInnerBox,
  drawerContentBottomText,
  drawerContentBottomName,
  listItemButton,
  listItemText,
} from './StyleSideBar';
import SideBarButtons from './SideBarButtons';
import TeamsBar from '../TeamsBar/TeamsBar';
import { updateUserBadges } from '../../services/user.services';

const drawerWidthOpen = 240;
const paddingIconButton = 10;
const marginIconButton = 14;
const iconFontSize = 20;
const drawerWidthClose =
  (paddingIconButton + marginIconButton) * 2 + iconFontSize;

const SideBar = ({ badgesFromNotif }: any) => {
  const theme = useTheme();
  const [open, setOpen] = useState(false);
  const { user, userData, setContext } = useContext(AuthContext);
  const [diffBadges, setDiffBadges] = useState(0);
  const navigate = useNavigate();

  const notificationsButton = {
    id: 6,
    path: 'notifications',
    icon: NotificationsActiveIcon,
    desc: 'Notifications',
    secondDesc: '',
    badge: diffBadges,
    subList: [],
  };

  const logout = () => {
    signOut(auth).then(() => {
      setContext({ user: null, userData: null });

      navigate('/');
    });
  };
  useEffect(() => {
    userData && setDiffBadges(badgesFromNotif - Number(userData?.badges));
  }, [badgesFromNotif, userData]);

  function toogleOpen() {
    setOpen(!open);
  }

  const checkPath = (path: string) => {
    if (path !== 'owned-teams' && path !== 'all-teams') {
      return true;
    }
    return false;
  };

  const drawerContent = (
    <>
      <Box sx={{ ...drawerContentStyle }}>
        {/* <Box sx={{ ...drawerContentOpenClose }}></Box> */}
        <Typography
          variant='h1'
          noWrap={true}
          gutterBottom
          sx={{ ...drawerContentTextTitle }}
        ></Typography>
        <Button onClick={toogleOpen} sx={{ ...drawerContentButtonOpenClose }}>
          <MenuIcon
            sx={{ fontSize: '20px', color: open ? 'lightgray' : 'lightGray' }}
          ></MenuIcon>
        </Button>
      </Box>
      <List dense={true}>
        {/* predi map na butonite hard coded notif */}

        <Link
          style={{ textDecoration: 'none' }}
          key={notificationsButton.id}
          to={notificationsButton.path}
        >
          <ListItemButton
            onClick={() => {
              const data = { ...userData, badges: badgesFromNotif };
              if (userData && userData.uid) {
                updateUserBadges(userData?.uid, badgesFromNotif);
                setContext({ user, userData: data });
              }
            }}
            sx={{ ...listItemButton }}
          >
            <ListItemIcon sx={{ minWidth: '46px' }}>
              <Badge
                badgeContent={notificationsButton.badge}
                color='secondary'
                variant='dot'
              >
                <notificationsButton.icon
                  sx={{ fontSize: '20px', color: 'lightgray' }}
                />
              </Badge>
            </ListItemIcon>

            <ListItemText
              primary={notificationsButton.desc}
              primaryTypographyProps={{
                variant: 'body2',
              }}
              sx={{ ...listItemText }}
            />
            {notificationsButton.badge !== 0 ? (
              <Chip
                label={notificationsButton.badge}
                color={'secondary'}
                size='small'
                sx={{ height: 'auto' }}
              />
            ) : (
              <></>
            )}
          </ListItemButton>
        </Link>

        {/* ostanalite butoni #26284687 */}
        {SideBarButtons.map((buttonInfo) =>
          checkPath(buttonInfo.path) ? (
            <Link
              style={{ textDecoration: 'none' }}
              key={buttonInfo.id}
              to={buttonInfo.path}
            >
              <ListItemButton sx={{ ...listItemButton }}>
                <ListItemIcon sx={{ minWidth: '46px' }}>
                  <Badge
                    badgeContent={buttonInfo.badge}
                    color='secondary'
                    variant='dot'
                  >
                    <buttonInfo.icon
                      sx={{ fontSize: '20px', color: 'lightgray' }}
                    />
                  </Badge>
                </ListItemIcon>

                <ListItemText
                  primary={buttonInfo.desc}
                  primaryTypographyProps={{
                    variant: 'body2',
                  }}
                  sx={{ ...listItemText }}
                />
              </ListItemButton>
            </Link>
          ) : (
            <TeamsBar key={buttonInfo.id} {...buttonInfo} />
          )
        )}
        <Divider variant='middle' light={true} />
      </List>
      <Box sx={{ ...drawerContentBottomStyle }}>
        <Box sx={{ ...drawerContentBottomStyleInnerBox }}>
          <Avatar
            variant='rounded'
            key={Date.now()}
            src={userData?.avatar ? userData.avatar : '/broken-image.jpg'}
            sx={{ width: '50px', height: '50px' }}
          ></Avatar>
        </Box>
        <Box sx={{ display: 'flex', flexDirection: 'column', flexGrow: 1 }}>
          <Typography
            component='span'
            variant='body2'
            sx={{ ...drawerContentBottomName }}
          >
            {userData?.username}
          </Typography>
          <Typography
            component='span'
            variant='body2'
            sx={{ ...drawerContentBottomText }}
          >
            {userData?.userRole}
          </Typography>
        </Box>
        <Button onClick={logout}>
          <LogoutIcon />
        </Button>
      </Box>
    </>
  );

  return (
    <Box sx={{ display: 'flex' }}>
      <Drawer
        variant='permanent'
        open={open}
        sx={{
          width: open
            ? { xs: '0px', sm: drawerWidthClose }
            : { xs: drawerWidthClose, sm: drawerWidthOpen },
          transition: theme.transitions.create('width', {
            // easing: theme.transitions.easing.sharp,
            // duration: open
            //   ? theme.transitions.duration.leavingScreen
            //   : theme.transitions.duration.enteringScreen,
          }),
          '& .MuiDrawer-paper': {
            justifyContent: 'space-between',
            // overflowX: 'hidden',
            width: open
              ? { xs: '0px', sm: drawerWidthClose }
              : { xs: drawerWidthClose, sm: drawerWidthOpen },
            borderRight: '0px',
            borderRadius: '0px 10px 10px 0px',
            boxShadow: theme.shadows[8],
            backgroundColor: open ? '#11101D' : '#11101D',
            transition: theme.transitions.create('width', {
              // easing: theme.transitions.easing.sharp,
              // duration: open
              //   ? theme.transitions.duration.leavingScreen
              //   : theme.transitions.duration.enteringScreen,
            }),
          },
        }}
      >
        {drawerContent}
      </Drawer>
    </Box>
  );
};

export default SideBar;
