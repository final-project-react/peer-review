import { Button, Modal, Tooltip, Typography } from '@mui/material';
import { Box } from '@mui/system';
import GroupRemoveIcon from '@mui/icons-material/GroupRemove';
import React, { useState } from 'react';
import PersonRemoveIcon from '@mui/icons-material/PersonRemove';
const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '20vw',
  height: '10vh',
  bgcolor: 'white',
  boxShadow: 24,
  p: 4,
  overflowX: 'auto',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  borderRadius: '10px'
};

const ModalDeleteUser = (props: any) => {
  const [open, setOpen] = useState(false);
  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => setOpen(false);

  return (
    <>
      <Button onClick={handleOpen} sx={{ fontSize: 14 }}>
        <Tooltip title='Remove user'>
          <PersonRemoveIcon />
        </Tooltip>
      </Button>
      <Modal keepMounted open={open} onClose={handleClose}>
        <Box sx={style}>
          <Typography variant='h6' sx={{alignSelf: 'center'}}>Do you want to delete that user?</Typography>
          <Box sx={{ display: 'flex', justifyContent: 'center', paddingTop: '20px', gap: '5px'}}>
            <Tooltip title='Delete user'>
              <Button variant='outlined' color='success' sx={{ width: 25 }} onClick={props.removeUser}>
                Yes
              </Button>
            </Tooltip>
            <Tooltip title='Cancel'>
              <Button variant='outlined' color='error' sx={{ width: 25 }} onClick={handleClose}>
                No
              </Button>
            </Tooltip>
          </Box>
        </Box>
      </Modal>
    </>
  );
};

export default ModalDeleteUser;
