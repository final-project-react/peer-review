import {
  Box,
  Button,
  Chip,
  Grid,
  TextField,
  Typography,
  Tooltip,
} from '@mui/material';
import UploadFileIcon from '@mui/icons-material/UploadFile';
import React, { ChangeEvent, useRef } from 'react';
import { gridSpacing, mainFont } from '../../ui-helpers/ui-helpers';
import MenuItem from '@mui/material/MenuItem';
import { useState } from 'react';
import EditorCreateResearch from '../EditorCreateResearch/EditorCreateResearch';

import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { useContext } from 'react';
import { AuthContext, UserData } from '../../Context/Context';
import { createResearch } from '../../services/researchItem';
import { useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { pushNotify } from '../../notify/notify';
import {
  getDownloadURL,
  ref as storageRef,
  uploadBytes,
} from 'firebase/storage';
import { storage } from '../../firebase/firebaseConfig';
import {
  createNotification,
  findUserIdByEmail,
  getUserData,
} from '../../services/user.services';
import { TaskButtonText } from '../../constants/team.const';

const priorities = [
  {
    value: 'High',
    label: 'High',
  },
  {
    value: 'Mid',
    label: 'Mid',
  },
  {
    value: 'Low',
    label: 'Low',
  },
];

const styleFields = {
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-around',
  alignItems: 'center',
};

const textFieldSize = {
  width: '250px',
};

const titleSize = {
  width: '120px',
  fontFamily: mainFont,
  color: 'black',
  borderBottom: ' 1px double #B2B1B1',
};

export interface FormObject {
  avatar?: string;
  title: string;
  content: {};
  assignee: string;
  assigneeAvatar: string | undefined;
  reporter: string;
  dueDate: string;
  createdOn: string;
  priority: string;
  team: string;
  uid: string | undefined;
  creator: string;
  status: string;
  teamId: string | undefined;
  file: string;
}

const CreateResearch = (props: any) => {
  const { userData } = useContext(AuthContext);
  const { id } = useParams<'id'>();
  const [editor, setEditor] = useState(false);
  const [members, setMembers] = useState<any[]>();
  const [file, setFile] = useState<string>();
  const [fileName, setFileName] = useState<string>();
  const hiddenFileInput = useRef<HTMLInputElement>(null);
  const [form, setForm] = useState<FormObject>({
    avatar: userData?.avatar,
    title: '',
    content: '',
    assignee: '',
    assigneeAvatar: '',
    reporter: `${userData?.username}`,
    dueDate: '',
    createdOn: new Date(Date.now()).toLocaleDateString(),
    priority: '',
    team: props.currentTeam?.name,
    uid: userData?.uid,
    creator: `${userData?.uid}`,
    status: 'Pending',
    teamId: id,
    file: '',
  });

  const updatePost =
    (prop: string, setForm: any, form: {}) =>
    (e: ChangeEvent<HTMLInputElement>) => {
      setForm({
        ...form,
        [prop]: e.target.value,
      });
    };

    useEffect(() => {
      findUserIdByEmail(form.assignee).then(user => {
        if (user?.avatar) {
          setForm({...form, 'assigneeAvatar': user?.avatar})
        } else {
          setForm({...form, 'assigneeAvatar': 'empty'})
        }
      });
    }, [form.assignee])

  const handleContent = (prop: string, delta: {}) => {
    setForm({
      ...form,
      [prop]: delta,
    });
    setEditor(true);
  };

  useEffect(() => {

    getUserData(props.currentTeam.ownerId).then((user) => {
      const ownerTeamUser: UserData[] = Object.values(user.val());
      const currentMembers = Object.entries(props.currentTeam.members).filter(
        (member) => member[0] !== userData?.uid
      );
      if (ownerTeamUser[0].uid !== userData?.uid) {
        setMembers([
          ...currentMembers,
          [ownerTeamUser[0].uid, ownerTeamUser[0].email],
        ]);
      } else {
        setMembers(currentMembers);
      }
    });
  }, [props.currentTeam, userData?.uid]);

  useEffect(() => {
    console.log(form);
    if (form.content) {
      createResearch(
        form.avatar,
        form.title,
        form.content,
        form.assignee,
        form.assigneeAvatar,
        form.reporter,
        form.dueDate,
        form.createdOn,
        form.priority,
        form.team,
        form.uid,
        form.creator,
        form.status,
        form.teamId,
        form.file
      )
        .then(() => {
          props.setCurrentView(TaskButtonText.MY_WORK_ITEMS, 2);

          pushNotify(
            'success',
            'Success',
            'Good luck your Research been assigned'
          );
        })
        .catch((e) => pushNotify('error', 'Error', `${e.message}`));

      const data = {
        user: userData?.username,
        userId: userData?.uid,
        type: 'Team',
        notification: `You been assigned to new Research  in team: ${form.team}`,
        teamId: form.teamId,
        date: Date.now(),
      };
      const assignedTo = members?.filter(
        (member) => member[1] === form.assignee
      )[0][0];
      console.log(assignedTo);

      createNotification(assignedTo, data);
    }
  }, [editor]);

  const deleteFile = () => {
    setFile(undefined);
    setForm({
      ...form,
      file: '',
    });
  };

  const handleChange = (e: any) => {
    const file = e.target.files[0];
    const checkValue = file?.name.split('.');
    if (file) {
      if (
        checkValue[checkValue.length - 1] !== 'pdf' &&
        checkValue[checkValue.length - 1] !== 'word' &&
        checkValue[checkValue.length - 1] !== 'docx' &&
        checkValue[checkValue.length - 1] !== 'zip'
      ) {
        return pushNotify(
          'error',
          'Error',
          'You can upload only pdf word docx zip file'
        );
      }
      setFileName(checkValue.join('.'));
      const fileToStorage = storageRef(
        storage,
        `files/${userData?.username}/${userData?.uid}`
      );

      uploadBytes(fileToStorage, file).then((snapshot) => {
        getDownloadURL(snapshot.ref).then((result) => {
          setFile(result);

          setForm({
            ...form,
            file: result,
          });
        });
      });
    }
  };

  const handleClick = (event: any) => {
    if (hiddenFileInput.current) {
      hiddenFileInput.current.click();
    }
  };

  return (
    <Grid
      container
      spacing={gridSpacing}
      sx={{ paddingLeft: '30px', width: '100%', height: '88vh' }}
    >
 

      <Grid
        item
        xs={7}
        sx={{ display: 'flex', flexDirection: 'column-reverse' }}
      >
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
          }}
        >
          <TextField
            required
            onChange={updatePost('title', setForm, form)}
            label='Title'
            variant='standard'
            placeholder='Title field'
            sx={{ width: '35%' }}
          />
          <input
            style={{ display: 'none' }}
            onChange={handleChange}
            ref={hiddenFileInput}
            type='file'
            id='fileInput'
            name='attachement'
          />
          <span style={{ display: 'flex', flexDirection: 'row' }}>
            <Tooltip title='Upload a file'>
              <Button onClick={handleClick} type='submit'>
                <UploadFileIcon
                  sx={{ width: '30px', height: '30px', alignItems: 'flex-end' }}
                />
              </Button>
            </Tooltip>
          </span>
          {file && (
            <Chip
              label={fileName}
              color='success'
              onDelete={deleteFile}
              sx={{ height: '100%' }}
            />
          )}
        </Box>
      </Grid>

      <Grid item xs={7} sx={{ height: '75vh' }}>
        <Box width='auto'>
          <EditorCreateResearch handleContent={handleContent} form={form} />
        </Box>
      </Grid>

      <Grid item xs={5}>
        <Box sx={{ height: '75vh' }}>
          <Grid
            container
            spacing={gridSpacing}
            sx={{ padding: '30px', paddingTop: '15px', gap: '6px' }}
          >
            <Grid item xs={12} sx={styleFields}>
              <Typography variant='h6' sx={titleSize}>
                Assignee
              </Typography>
              <TextField
                select
                required
                variant='standard'
                onChange={updatePost('assignee', setForm, form)}
                value={form.assignee}
                label='Reviewer'
                helperText='Please select your reviewer'
                sx={textFieldSize}
              >
                {members?.map((option) => (
                  <MenuItem key={option[1]} value={option[1]}>
                    {option[1]}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>

            {/* <Grid item xs={12} sx={styleFields}>
              <Typography variant='h6' sx={titleSize}>
                Reporter
              </Typography>
              <TextField
                value={userData?.username}
                label='Read Only'
                variant='standard'
                sx={textFieldSize}
                helperText='Default reporter'
              />
            </Grid> */}

            <Grid item xs={12} sx={styleFields}>
              <Typography variant='h6' sx={titleSize}>
                Due Date
              </Typography>
              <LocalizationProvider dateAdapter={AdapterMoment}>
                <TextField
                  required
                  onChange={updatePost('dueDate', setForm, form)}
                  label='Due Date'
                  type='date'
                  variant='standard'
                  helperText='Please select due date'
                  sx={textFieldSize}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </LocalizationProvider>
            </Grid>

            <Grid item xs={12} sx={styleFields}>
              <Typography variant='h6' sx={titleSize}>
                Priority
              </Typography>
              <TextField
                required
                select
                onChange={updatePost('priority', setForm, form)}
                value={form.priority}
                label='Priority'
                variant='standard'
                helperText='Please select priority'
                sx={textFieldSize}
              >
                {priorities.map((option) => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>

            <Grid item xs={12} sx={styleFields}>
              <Typography variant='h6' sx={titleSize}>
                Team
              </Typography>
              <TextField
                value={props.currentTeam.name}
                label='Read Only'
                variant='standard'
                helperText='Current team'
                sx={textFieldSize}
              />
            </Grid>

            <Grid item xs={12} sx={styleFields}>
              <Typography variant='h6' sx={titleSize}>
                Creator
              </Typography>
              <TextField
                value={userData?.username}
                sx={textFieldSize}
                label='Read Only'
                variant='standard'
                helperText='Creator of the item'
              />
            </Grid>

            <Grid item xs={12} sx={styleFields}>
              <Typography variant='h6' sx={titleSize}>
                Status
              </Typography>
              <TextField
                value={'Pending'}
                sx={textFieldSize}
                label='Default status'
                variant='standard'
                helperText='Default status is provided'
              />
            </Grid>
          </Grid>
        </Box>
      </Grid>
    </Grid>
  );
};

export default CreateResearch;
