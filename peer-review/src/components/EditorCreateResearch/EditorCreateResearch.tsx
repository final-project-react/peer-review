import { Box, Button } from '@mui/material';
import { useCallback } from 'react';
import { useState } from 'react';
import { Quill } from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import { pushNotify } from '../../notify/notify';
import './editor.css';

const modules = [
  [{ header: [1, 2, 3, 4, 5, 6, false] }],
  [{ font: [] }],
  [{ size: [] }],
  ['bold', 'italic', 'underline', 'strike', 'blockquote'],
  [{ list: 'ordered' }, { list: 'bullet' }, { indent: '-1' }, { indent: '+1' }],
  ['link', 'image', 'video'],
  ['clean', 'code-block',],
];

interface prop {
  handleContent: (prop: string, delta: {}) => void;
  form: {};
}

const EditorCreateResearch = ({ handleContent, form }: prop) => {
  const [quill, setQuill] = useState<any | null>(null);

  const getEditorContentOnClick = () => {
    for (const [key, value] of Object.entries(form)) {
      if (
        value === '' &&
        key !== 'content' &&
        key !== 'avatar' &&
        key !== 'file'
      ) {
        return pushNotify(
          'warning',
          `Warning`,
          `You have empty filed - ${key.toUpperCase()}`
        );
      }
    }
    handleContent('content', quill.getContents());
  };


  const wrapperRef = useCallback((wrapper: HTMLElement) => {
    if (wrapper === null) return;

    wrapper.innerHTML = '';
    const editor = document.createElement('Box');
    wrapper.append(editor);
    const newEditor = new Quill(editor, {
      theme: 'snow',
      modules: { toolbar: modules,},
    });
    setQuill(newEditor);
  }, []);

  return (
    <Box id='editor-container'>
      <Box id='container' ref={wrapperRef}></Box>
      <Button
        variant='outlined'
        sx={{ width: '100%',
        color: 'white',
        backgroundColor:'blue',
        textTransform: 'none', 
        "&:hover": { backgroundColor: "blue"},
        boxShadow: 'rgba(0, 0, 0, 0.35) 0px -50px 36px -28px inset'
      }}
        onClick={getEditorContentOnClick}
      >
        Publish
      </Button>
    </Box>
  );
};

export default EditorCreateResearch;
