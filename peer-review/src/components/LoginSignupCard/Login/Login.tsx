import React, { useState } from 'react';
import { Box, Button, Container, TextField, Typography } from '@mui/material';
import { pushNotify } from '../../../notify/notify';
import { useContext } from 'react';
import { AuthContext } from '../../../Context/Context';
import { signInWithEmailAndPassword } from '@firebase/auth';
import { auth } from '../../../firebase/firebaseConfig';
import { getUserData } from '../../../services/user.services';
import { useNavigate } from 'react-router-dom';
import { mainFont } from '../../../ui-helpers/ui-helpers';

const style = {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-evenly',
  width: '100%',
  height: '700px',
  borderRadius: '50px',
  backgroundColor: '#BBCEE0',
  perspective: '1000px',
  boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
};
interface Form {
  email: string;
  password: string;
}

// interface LoginProps {
//   handleFlip: () => void;
// }

const Login = (props: any) => {
  const { setContext } = useContext(AuthContext);
  const [form, setForm] = useState<Form>({
    email: '',
    password: '',
  });

  const navigate = useNavigate();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setForm({ ...form, [name]: value });
  };

  const singIn = () => {
    signInWithEmailAndPassword(auth, form.email, form.password)
      .then((user) => {
        return getUserData(user.user.uid)
          .then((snapshot) => {
            if (snapshot.exists()) {
              setContext({
                user: user.user,
                userData: snapshot.val()[Object.keys(snapshot.val())[0]],
              });

              pushNotify('success', 'Success', `Welcome ${form.email}`);

              navigate('assignments');
              //TODO: user can't go back to home page
            }
          })
          .catch((e) => pushNotify('error', 'Error', `${e.message}`));
      })
      .catch((error) => {
        pushNotify('error', 'Error', `${error.message}`);
      });
  };
  return (
    <Container sx={style}>
      <Typography
        variant='h4'
        align='center'
        sx={{
          fontFamily: mainFont,
          fontSize: '35px',
          fontStyle: 'normal',
          color: '#4C1DD3',
        }}
      >
        SIGN IN
      </Typography>

      <Button
        onClick={props.handleFlip}
        sx={{
          display: 'flex',
          alignSelf: 'center',
          height: '50px',
          color: '#4C1DD3',
          width: '50%',
        }}
      >
        <span
          style={{
            color: 'black',
            fontFamily: mainFont,
            fontStyle: 'normal',
          }}
        >
          Don't have account yet?
          <Typography variant='body1' sx={{ color: '#4C1DD3' }}>
            Sign up here
          </Typography>
        </span>
      </Button>

      <Typography
        variant='h4'
        sx={{
          display: 'flex',
          alignSelf: 'center',
          color: 'black',
          fontFamily: mainFont,
          fontStyle: 'normal',
        }}
      >
        Welcome Back
      </Typography>
      <Box
        sx={{
          display: ' flex',
          flexDirection: 'column',
          justifyContent: 'space-evenly',
          alignSelf: 'center',
          height: '45%',
          width: '80%',
        }}
      >
        <TextField
          label='Email'
          type='email'
          name='email'
          variant='standard'
          onChange={handleChange}
        />
        <TextField
          label='Password'
          type='password'
          name='password'
          variant='standard'
          onChange={handleChange}
        />
      </Box>

      <Button
        variant='contained'
        sx={{
          display: 'flex',
          alignSelf: 'center',
          backgroundColor: '#4C1DD3',
          borderRadius: '40px',
          width: '50%',
        }}
        onClick={() => singIn()}
      >
        <Typography
          variant='h4'
          sx={{ fontFamily: mainFont, fontStyle: 'normal', fontSize: '180%' }}
        >
          Sign In
        </Typography>
      </Button>

      <Typography variant='body1' align='center'>
        <Button sx={{ color: '#4C1DD3' }}>Forgot password?</Button>
      </Typography>
    </Container>
  );
};

export default Login;
