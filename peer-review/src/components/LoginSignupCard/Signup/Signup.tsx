import { createUserWithEmailAndPassword } from '@firebase/auth';
import { Box, Button, Container, TextField, Typography } from '@mui/material';
import React, { useContext } from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { AuthContext, UserData } from '../../../Context/Context';
import { auth } from '../../../firebase/firebaseConfig';
import { pushNotify } from '../../../notify/notify';
import { createNotification, createUserData, getUserData } from '../../../services/user.services';
import { mainFont } from '../../../ui-helpers/ui-helpers';
import { validate } from './validation';

const style = {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-evenly',
  width: '100%',
  height: '700px',
  borderRadius: '50px',
  backgroundColor: '#BBCEE0',
  perspective: '1000px',
  boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
};

// interface SignupProps {
//   handleFlip: () => void;
// }

const Signup = (props: any) => {
  const [formValues, setFormValues] = useState({
    firstName: '',
    lastName: '',
    username: '',
    email: '',
    phoneNumber: '',
    password: '',
  });
  const [formErrors, setFormErrors] = useState<any>({});
  const [isSubmit, setIsSubmit] = useState(false);
  const { setContext } = useContext(AuthContext);
  const navigate = useNavigate();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleSubmit = () => {
    setFormErrors(validate(formValues));
    setIsSubmit(true);
  };


  const createUser: any = () => {
    createUserWithEmailAndPassword(auth, formValues.email, formValues.password)
      .then((user) => {
        createUserData(
          formValues.firstName,
          formValues.lastName,
          formValues.username,
          formValues.email,
          formValues.phoneNumber,
          user.user.uid
        ).then(() => {
          pushNotify(
            'success',
            'Success',
            'The account has been successfully created'
          );
          getUserData(user.user.uid).then((data) => {
            setContext({
              user: user.user,
              userData: data.val()[Object.keys(data.val())[0]],
            });
            const dataNotification = {
              notification: `Welcome on board ${formValues.firstName}`
            }
            createNotification(user.user.uid, dataNotification);

          });
          navigate('assignments');
        });
       
      })
      .catch((error) => {
        if (error.message.includes('email-already-in-use')) {
          pushNotify('error', 'Error', 'Email already in use');
        } else {
          pushNotify('error', 'Error', 'Invalid email');
        }
      });
  };

  useEffect(() => {
    if (Object.keys(formErrors).length === 0 && isSubmit) {
      createUser();
    }
  }, [formErrors, isSubmit]);


  return (
    <Container sx={style}>
      <Typography
        variant='h4'
        align='center'
        sx={{
          fontFamily: mainFont,
          fontSize: '35px',
          fontStyle: 'normal',
          color: '#4C1DD3',
        }}
      >
        SIGN UP
      </Typography>

      <Button
        onClick={props.handleFlip}
        sx={{
          display: 'flex',
          alignSelf: 'center',
          height: '50px',
          color: '#4C1DD3',
          width: '50%',
        }}
      >
        <Typography
          variant='body1'
          sx={{ fontFamily: mainFont, fontStyle: 'normal' }}
        >
          Already have an account ?
        </Typography>
      </Button>

      <Box
        sx={{
          display: ' flex',
          flexDirection: 'row',
          justifyContent: 'space-around',
          height: '5%',
        }}
      >
        <TextField
          error={formErrors.firstName ? true : false}
          helperText={formErrors.firstName ? 'First Name Required' : ''}
          label='First Name'
          defaultValue=''
          variant='standard'
          name='firstName'
          onChange={handleChange}
        />

        <TextField
          error={formErrors.lastName ? true : false}
          helperText={formErrors.lastName ? 'Last Name Required' : ''}
          label='Last Name'
          defaultValue=''
          variant='standard'
          name='lastName'
          onChange={handleChange}
        />
      </Box>

      <Box
        sx={{
          display: ' flex',
          flexDirection: 'column',
          justifyContent: 'space-evenly',
          alignSelf: 'center',
          height: '50%',
          width: '80%',
        }}
      >
        <TextField
          error={formErrors.username ? true : false}
          helperText={formErrors.username ? 'Username is Required' : ''}
          label='Username'
          defaultValue=''
          variant='standard'
          name='username'
          onChange={handleChange}
        />
        <TextField
          error={formErrors.email ? true : false}
          helperText={formErrors.email ? 'Email is Required' : ''}
          label='Email'
          defaultValue=''
          variant='standard'
          name='email'
          onChange={handleChange}
        />
        <TextField
          error={formErrors.phoneNumber ? true : false}
          helperText={formErrors.phoneNumber ? 'Phone Number is Required' : ''}
          label='Number'
          type='number'
          variant='standard'
          name='phoneNumber'
          onChange={handleChange}
        />
        <TextField
          error={formErrors.password ? true : false}
          helperText={formErrors.password ? 'Password is Required' : ''}
          label='Password'
          type='password'
          variant='standard'
          name='password'
          onChange={handleChange}
        />
      </Box>
      <Button
        variant='contained'
        sx={{
          display: 'flex',
          alignSelf: 'center',
          backgroundColor: '#4C1DD3',
          borderRadius: '40px',
          width: '60%',
          height: '60px',
        }}
        onClick={() => handleSubmit()}
      >
        <Typography
          variant='h4'
          sx={{ fontFamily: mainFont, fontStyle: 'normal', fontSize: '160%' }}
        >
          Get Started
        </Typography>
      </Button>
    </Container>
  );
};

export default Signup;
