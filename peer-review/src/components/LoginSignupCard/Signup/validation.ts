export const validate = (values : any) => {

  const errors: any = {};
  const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
  
  if (!values.firstName) {
    errors.firstName = 'First Name is required'
  }

  if (!values.lastName) {
    errors.lastName = 'Last Name is required'
  }

  if (!values.username) {
    errors.username = "Username is required!";
  }
  if (!values.email) {
    errors.email = "Email is required!";
  } else if (!regex.test(values.email)) {
    errors.email = "This is not a valid email format!";
  }

  if (!values.phoneNumber) {
    errors.phoneNumber = "Phone Number is required!"
  }

  if (!values.password) {
    errors.password = "Password is required";
  } else if (values.password.length < 4) {
    errors.password = "Password must be more than 4 characters";
  } else if (values.password.length > 10) {
    errors.password = "Password cannot exceed more than 10 characters";
  }
  return errors;
};
