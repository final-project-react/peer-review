import React, { useState } from 'react';
import ReactCardFlip from 'react-card-flip';
import Login from './Login/Login';
import Signup from './Signup/Signup';

const LoginSignupCard = () => {
  const [flipped, setFlipped] = useState<boolean>(false);

  const handleFlip = () => {
    setFlipped(!flipped);
  };

  return (
    <ReactCardFlip isFlipped={flipped} flipDirection='horizontal'>
      <Login handleFlip={handleFlip} />
      <Signup handleFlip={handleFlip} />
    </ReactCardFlip>
  );
};

export default LoginSignupCard;
