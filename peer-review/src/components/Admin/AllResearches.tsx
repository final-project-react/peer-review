import { Button } from '@mui/material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import {
  deleteItem,
  getAllResearches,
  removeFromUserItem,
} from '../../services/researchItem';
import { ResearchItem } from '../Interfaces/Interfaces';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';

const AllResearches = () => {
  const [researches, setResearches] = useState<ResearchItem[]>([]);
  const [searchCriteria, setSearchCriteria] = useState<string>('');

  useEffect(() => {
    getAllResearches().then((res) => {
      if (res.val()) {
        const entrResearches: [string, ResearchItem][] = Object.entries(
          res.val()
        );
        const mappedResearches: ResearchItem[] = entrResearches.map((team) => ({
          id: team[0],
          ...team[1],
        }));
        setResearches(mappedResearches);
      }
    });
  }, []);

  const deleteResearch = (itemId: any, uid: string) => {
    deleteItem(itemId);
    removeFromUserItem(itemId, uid);
    const newResearches = researches.filter((res) => res.id !== itemId);
    setResearches(newResearches);
  };

  const getFilteredResearches = () => {
    return researches.filter((res) => {
      if (!searchCriteria) {
        return true;
      }
      return res.title.toLowerCase().includes(searchCriteria.toLowerCase());
    });
  };

  return (
    <TableContainer sx={{ height: '50vh', backgroundColor: 'whitesmoke' }}>
      <input
        placeholder='Search here...'
        style={{
          minWidth: 650,
          width: 825,
          height: 36,
          borderRadius: '4px',
          border: '1px solid grey',
          margin: 2,
        }}
        onChange={(searchVal) => setSearchCriteria(searchVal.target.value)}
      />
      <Table
        sx={{ minWidth: 650 }}
        stickyHeader
        size='medium'
        aria-label='a dense table'
      >
        <TableHead>
          <TableRow sx={{ borderBottom: 1 }}>
            <TableCell sx={{ fontWeight: 'bold' }}>Research</TableCell>

            <TableCell sx={{ fontWeight: 'bold' }} align='center'>
              Created by
            </TableCell>
            <TableCell sx={{ fontWeight: 'bold' }} align='center'>
              Team
            </TableCell>
            <TableCell sx={{ fontWeight: 'bold' }} align='center'>
              Created on
            </TableCell>
            <TableCell sx={{ fontWeight: 'bold' }} align='center'>
              Action
            </TableCell>
            {/* <TableCell align='right'>content</TableCell> */}
          </TableRow>
        </TableHead>
        <TableBody>
          {getFilteredResearches().map((res) => (
            <TableRow
              key={res.id}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component='th' scope='row'>
                <NavLink to={`/research-item/${res.id}`}>{res.title}</NavLink>
              </TableCell>
              <TableCell align='center'>{res.reporter}</TableCell>
              <TableCell align='center'>{res.team}</TableCell>
              <TableCell align='center'>{res.createdOn}</TableCell>
              <TableCell align='center'>
                <Button onClick={() => deleteResearch(res?.id, res.creator)}>
                  <DeleteForeverIcon />
                </Button>
              </TableCell>

              {/* <TableCell align='right'>{post[1].context}</TableCell> */}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default AllResearches;
