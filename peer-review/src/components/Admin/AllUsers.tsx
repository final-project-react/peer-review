import { Button } from '@mui/material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import React, { useContext, useEffect, useState } from 'react';
import { AuthContext, UserData } from '../../Context/Context';
import { getAllUsers, updateUserRole } from '../../services/user.services';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';

const AllUsers = () => {
  const [users, setUsers] = useState<UserData[]>([]);
  const [searchCriteria, setSearchCriteria] = useState<string>('');
  const { userData } = useContext(AuthContext);
  const [render, setRender] = useState(false);

  useEffect(() => {
    getAllUsers().then((users) => {
      if (users.val()) {
        const entrUsers: UserData[] = Object.values(users.val());
        const filteredUsers = entrUsers.filter(
          (user) => user.uid !== userData?.uid
        );
        setUsers(filteredUsers);
      }
    });
  }, [userData?.uid, render]);

  const promoteAdmin = (uid: string) => {
    updateUserRole(uid, 'admin');
    setRender(!render);
  };

  const demoteAdmin = (uid: string) => {
    updateUserRole(uid, 'member');
    setRender(!render);
  };

  const getFilteredUsers = () => {
    return users.filter((user) => {
      if (!searchCriteria) {
        return true;
      }
      if (user.username && user.email) {
        return (
          user.username.toLowerCase().includes(searchCriteria.toLowerCase()) ||
          user.email.toLowerCase().includes(searchCriteria.toLowerCase())
        );
      }
    });
  };

  return (
    <TableContainer sx={{ height: '50vh', backgroundColor: 'whitesmoke' }}>
      <input
        placeholder='Search here...'
        style={{
          minWidth: 650,
          width: 825,
          height: 36,
          borderRadius: '4px',
          border: '1px solid grey',
          margin: 2,
        }}
        onChange={(searchVal) => setSearchCriteria(searchVal.target.value)}
      />
      <Table
        sx={{ minWidth: 650 }}
        stickyHeader
        size='medium'
        aria-label='a dense table'
      >
        <TableHead>
          <TableRow sx={{ borderBottom: 1 }}>
            <TableCell sx={{ fontWeight: 'bold' }}>User</TableCell>

            <TableCell sx={{ fontWeight: 'bold' }} align='center'>
              Email
            </TableCell>
            <TableCell sx={{ fontWeight: 'bold' }} align='center'>
              Names
            </TableCell>
            <TableCell sx={{ fontWeight: 'bold' }} align='center'>
              Action
            </TableCell>
            {/* <TableCell align='right'>content</TableCell> */}
          </TableRow>
        </TableHead>
        <TableBody>
          {getFilteredUsers().map((user) => (
            <TableRow
              key={user.uid}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component='th' scope='row'>
                {user.username}
              </TableCell>
              <TableCell align='center'>{user.email}</TableCell>
              <TableCell align='center'>{`${user.firstName} ${user.lastName}`}</TableCell>
              <TableCell align='center'>
                {user.userRole === 'member' && (
                  <Button
                    onClick={() => promoteAdmin(user.uid ? user.uid : '')}
                  >
                    promote admin
                  </Button>
                )}
                {user.userRole === 'admin' && (
                  <Button onClick={() => demoteAdmin(user.uid ? user.uid : '')}>
                    demote admin
                  </Button>
                )}
              </TableCell>

              {/* <TableCell align='right'>{post[1].context}</TableCell> */}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default AllUsers;
