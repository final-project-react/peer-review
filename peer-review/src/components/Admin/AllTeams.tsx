import { Button } from '@mui/material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import React, { useContext, useEffect, useState } from 'react';
import { AuthContext } from '../../Context/Context';
import { removeItemComents } from '../../services/coments.services';
import {
  deleteItem,
  getAllResearches,
  removeFromUserItem,
} from '../../services/researchItem';
import {
  deleteTeamById,
  getAllTeams,
  getTeamById,
} from '../../services/teams.services';
import {
  getAllUsers,
  removeOwnerUserTeam,
  removeTeamIdFromUser,
} from '../../services/user.services';
import { ResearchItem, Team } from '../Interfaces/Interfaces';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';

const AllTeams = () => {
  const [teams, setTeams] = useState<Team[]>([]);
  const [searchCriteria, setSearchCriteria] = useState<string>('');
  const { userData } = useContext(AuthContext);

  useEffect(() => {
    getAllTeams().then((teams) => {
      if (teams.val()) {
        const entrTeams: [string, Team][] = Object.entries(teams.val());
        const mappedTeams: Team[] = entrTeams.map((team) => ({
          id: team[0],
          ...team[1],
        }));
        setTeams(mappedTeams);
      }
    });
  }, []);

  const getFilteredTeams = () => {
    return teams.filter((team) => {
      if (!searchCriteria) {
        return true;
      }
      return team.name.toLowerCase().includes(searchCriteria.toLowerCase());
    });
  };

  const deleteTeamOnClick = async (id: string) => {
    if (id && userData?.uid) {
      const team = await getTeamById(id);
      if (team.val().members) {
        const members = [...Object.keys(team.val().members), userData.uid];

        const allUsersData = await getAllUsers();
        const allUsers = Object.keys(allUsersData.val());
        const allUsersIdFromTeam = allUsers.filter((userId) =>
          members.includes(userId)
        );
        allUsersIdFromTeam.forEach((userId) => {
          removeTeamIdFromUser(userId, id);
        });
      }
      await removeOwnerUserTeam(userData?.uid, id);

      const allItemsData = await getAllResearches();
      if (allItemsData.val()) {
        const allItems: [string, ResearchItem][] = Object.entries(
          allItemsData.val()
        );

        const allItemsFromTeam = allItems
          .filter((item) => item[1].teamId === id)
          .map((item) => {
            return { ...item[1], id: item[0] };
          });

        allItemsFromTeam.forEach((item) => {
          removeFromUserItem(item.creator, item.id);
          removeItemComents(item.id);
          deleteItem(item.id);
        });
      }
      deleteTeamById(id);
      const filteredTeams = teams.filter((team) => team.id !== id);
      setTeams(filteredTeams);
    }
  };

  return (
    <TableContainer
      sx={{
        height: '50vh',
        backgroundColor: 'whitesmoke',
        borderRadius: '8px',
      }}
    >
      <input
        placeholder='Search here...'
        style={{
          minWidth: 650,
          width: 825,
          height: 36,
          borderRadius: '4px',
          border: '1px solid grey',
          margin: 2,
        }}
        onChange={(searchVal) => setSearchCriteria(searchVal.target.value)}
      />
      <Table
        sx={{ minWidth: 650 }}
        stickyHeader
        size='medium'
        aria-label='a dense table'
      >
        <TableHead>
          <TableRow sx={{ borderBottom: 1 }}>
            <TableCell sx={{ fontWeight: 'bold' }}>Team</TableCell>

            <TableCell sx={{ fontWeight: 'bold' }} align='center'>
              Created by
            </TableCell>
            <TableCell sx={{ fontWeight: 'bold' }} align='center'>
              Created on
            </TableCell>
            <TableCell sx={{ fontWeight: 'bold' }} align='center'>
              Action
            </TableCell>
            {/* <TableCell align='right'>content</TableCell> */}
          </TableRow>
        </TableHead>
        <TableBody>
          {getFilteredTeams().map((team) => (
            <TableRow
              key={team.id}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component='th' scope='row'>
                {team.name}
              </TableCell>
              <TableCell align='center'>{team.ownerEmail}</TableCell>
              <TableCell align='center'>{team.createdOn}</TableCell>
              <TableCell align='center'>
                {
                  <Button
                    onClick={() => {
                      console.log(team.id);
                      if (team.id) {
                        deleteTeamOnClick(team.id);
                      }
                    }}
                  >
                    <DeleteForeverIcon />
                  </Button>
                }
              </TableCell>

              {/* <TableCell align='right'>{post[1].context}</TableCell> */}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default AllTeams;
