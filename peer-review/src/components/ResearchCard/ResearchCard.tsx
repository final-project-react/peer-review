import React from 'react';
import {
  Avatar,
  Box,
  Card,
  Chip,
  Grid,
  Tooltip,
  Typography,
} from '@mui/material';
import AttachFileIcon from '@mui/icons-material/AttachFile';
import { mainFont } from '../../ui-helpers/ui-helpers';
import { ResearchItem } from '../Interfaces/Interfaces';
import Moment from 'react-moment';
import { findUserIdByEmail } from '../../services/user.services';

interface Prop {
  post: ResearchItem;
}

const ResearchCard = ({ post }: Prop) => {
  // console.log(post);
  return (
    <Card
      sx={{
        margin: '8px',
        backgroundColor: '#D7E7EE',
        borderRadius: '10px',
        outline: 'none',
      }}
    >
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          gap: '20px',
          paddingLeft: '10px',
          paddingRight: '10px',
          alignItems: 'center',
          py: 1.5,
        }}
      >
        <Box
          sx={{ display: 'flex', alignSelf: 'flex-start', borderRadius: '50%' }}
        >
          <Box sx={{display: 'flex', justifyContent: 'space-between'}}>
            
            {
            post && 
              <Tooltip title={(new Date(post.dueDate)).valueOf() > (Date.now()).valueOf() ? 'Due date' : 'Expired'}>
                  <Chip 
                  label={<Moment to={post.dueDate}>{new Date(Date.now())}</Moment>}
                  size='small'
                  variant='outlined'
                  color={
                    new Date(post.dueDate).valueOf() > Date.now().valueOf()
                      ? 'success'
                      : 'error'
                  }
                />
              </Tooltip>
          }
            {post.file && (
              <Tooltip title='Attached file'>
                <AttachFileIcon />
              </Tooltip>
            )}
          </Box>
          {/* {'Due Date'} */}
        </Box>
        <Typography
          variant='h6'
          color='inherit'
          sx={{ alignSelf: 'flex-start' }}
        >
          {post && post.title}
          {/* {'title'} */}
        </Typography>
      </Box>
      <Grid container>
        <span
          style={{
            display: 'flex',
            width: '100%',
            paddingLeft: '10px',
            paddingBottom: '10px',
          }}
        >
          <Tooltip title={`${post.assignee}`}>
            <Avatar
              src={
                post?.assigneeAvatar !== 'empty'
                  ? post?.assigneeAvatar
                  : '/broken-image.jpg'
              }
            />
          </Tooltip>
          <Typography
            variant='subtitle2'
            sx={{
              display: 'flex',
              alignSelf: 'flex-end',
              paddingLeft: '8px',
              // fontFamily: mainFont,
            }}
          >
            {post && `Assigned to ${post.assignee}`}
            {/* {'Assignee to : '} */}
          </Typography>
        </span>
      </Grid>
    </Card>
  );
};

export default ResearchCard;
