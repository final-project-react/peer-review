import { Box, Typography } from '@mui/material'
import logo from '../../imageSrc/peertopeer.svg';

import React from 'react'
import { mainBackGroundColor} from '../../ui-helpers/ui-helpers';

const PageLocation = (prop: {currentPage: string}) => {
  return (
    <Box sx={{ display: 'flex', justifyContent: 'space-between', padding: 2, background: mainBackGroundColor}}>
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}
    >
      <Typography sx={{ fontSize: '45px'}}>{prop.currentPage}</Typography>
    </Box>
    <img
      style={{ height: '50px', paddingTop: '10px' }}
      src={logo}
      alt='logo'
    />
  </Box>
  )
}

export default PageLocation
