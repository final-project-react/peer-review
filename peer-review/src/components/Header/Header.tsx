import { Typography } from '@mui/material';
import { Box } from '@mui/system';
import { mainFont } from '../../ui-helpers/ui-helpers';
import logo from './../../imageSrc/peertopeer.svg';

const Header = ({ page }: any) => {
  return (
    <Box sx={{ display: 'flex', justifyContent: 'space-between', m: 2 }}>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Typography sx={{ fontSize: '45px' }}>
          {page}
        </Typography>
      </Box>
      <img
        style={{ height: '50px', paddingTop: '10px'}}
        src={logo}
        alt='logo'
      />
    </Box>
  );
};

export default Header;
