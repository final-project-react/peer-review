import React, { useContext, useEffect, useState } from 'react';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { Badge, ListItemText, Typography } from '@mui/material';
import { ListItemButton, ListItemIcon } from '@mui/material';
import { listItemButton, listItemText } from '../SideBar/StyleSideBar';
import GroupsIcon from '@mui/icons-material/Groups';
import { getAllTeams } from '../../services/teams.services';
import { AuthContext } from '../../Context/Context';
import { NavLink } from 'react-router-dom';

interface Team {
  createdOn: string;
  description: string;
  members: {};
  name: string;
  ownerId: string;
  id: string;
}

const ITEM_HEIGHT = 48;

const TeamsBar = (props: any) => {
  const { userData } = useContext(AuthContext);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const [isOpen, setIsOpen] = useState(false);
  const [Teams, setTeams] = React.useState<[] | Team[]>([]);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setIsOpen(!isOpen);
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  //
  useEffect(() => {
    getAllTeams().then((teams) => {
      const teamsData = teams.val();
      if (teamsData && userData) {
        const arrayOfTeams: [string, Team][] = Object.entries<Team>(teamsData);
        const filteredTeams = arrayOfTeams.reduce(
          (acc: Team[], team: [string, Team]) => {
            if (userData && userData.uid && team && team[1].members) {
              if (props.desc === 'My teams') {
                if (team[1].ownerId === userData.uid) {
                  const finalTeam = { ...team[1], id: team[0] };
                  return [...acc, finalTeam];
                }
              } else {
                if (userData.uid in team[1].members) {
                  const finalTeam = { ...team[1], id: team[0] };
                  return [...acc, finalTeam];
                }
              }
            }
            return acc;
          },
          []
        );
        setTeams(filteredTeams);
      }
    });
  }, [userData, props.desc, isOpen]); // dobavi

  return (
    <div style={{ color: 'black' }}>
      <ListItemButton
        sx={{ ...listItemButton }}
        aria-label='more'
        id='long-button'
        aria-controls={open ? 'long-menu' : undefined}
        aria-expanded={open ? 'true' : undefined}
        aria-haspopup='true'
        onClick={handleClick}
      >
        <ListItemIcon sx={{ minWidth: '46px' }}>
          <Badge badgeContent={props.badge} color='secondary' variant='dot'>
            <GroupsIcon sx={{ fontSize: '20px', color: 'lightgray' }} />
          </Badge>
        </ListItemIcon>
        <ListItemText
          primary={props.desc}
          primaryTypographyProps={{
            variant: 'body2',
          }}
          sx={{ ...listItemText }}
        />
      </ListItemButton>
      <Menu
        id='long-menu'
        MenuListProps={{
          'aria-labelledby': 'long-button',
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        PaperProps={{
          style: {
            color: 'white',
            backgroundColor: '#11101D',
            maxHeight: ITEM_HEIGHT * 4.5,
            width: '25ch',
          },
        }}
      >
        {Teams.length ? (
          Teams.map((team: Team) => (
            <NavLink
              key={team.id}
              style={{ textDecoration: 'none', color: 'white' }}
              to={`team/${team.id}`}
            >
              <MenuItem
                // selected={team === 'Pyxis'}
                onClick={handleClose}
              >
                {team.name}
              </MenuItem>
            </NavLink>
          ))
        ) : (
          <Typography>There is no teams</Typography>
        )}
      </Menu>
    </div>
  );
};

export default TeamsBar;
