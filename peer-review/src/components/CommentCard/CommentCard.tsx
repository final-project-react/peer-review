import { Avatar, Box, Card, CardContent, Chip, Tooltip, Typography } from '@mui/material';
import TagFacesIcon from '@mui/icons-material/TagFaces';
import ClearIcon from '@mui/icons-material/Clear';
import React from 'react';
import Moment from 'react-moment';

const CommentCard = ({comment, handleDeleteComment}: any) => {

  return (

    <Box sx={{display: 'flex', flexDirection: 'row',  marginTop: '20px', marginLeft: '5px'}}>
      <Box>
        <Tooltip title={<Box>{comment[1].username} <Moment fromNow>{comment[1].createdOn}</Moment></Box>}>

          <Avatar  sx={{width: '30px', height: '30px', backgroundColor: 'blue'}} ><TagFacesIcon/> </Avatar>
        </Tooltip>
      </Box>

      <Box sx={{display: 'flex', flexDirection: 'column', width: '100%'}}>

        <Box
          sx={{
            backgroundColor: 'white',
            borderRadius: '10px',
            outline: 'none',
            boxShadow: 'none',
            border: '1px solid #B2B1B1',
            height: 'auto',
            overflowWrap: 'anywhere',
            padding: '8px',
            width: '90%',
            borderTopLeftRadius: '0px',
            marginTop: '20px',
            marginLeft: '4px',
            display: 'flex',
            flexDirection: 'row',
          }}
        >
            <Typography sx={{minWidth:'95%'}}>{comment[1].content}</Typography>

              <Tooltip title='Delete'>
              <ClearIcon sx={{width: '15px', height: '15px', cursor: 'pointer'}} onClick={() => handleDeleteComment(comment[0])} />
            </Tooltip>

        </Box>
      </Box>
    </Box>
 
  );
};

export default CommentCard;
