export const asd = () => {
  return 'kolkoto da ne gurmi fail-a';
};

export interface ResearchItem {
  avatar: string;
  assignee: string;
  assigneeAvatar: string;
  content: {
    [key: string]: any;
  };
  createdOn: string;
  creator: string;
  dueDate: string;
  file?: string;
  priority: string;
  reporter: string;
  status: string;
  team: string;
  teamId: string;
  title: string;
  id?: string;
}

export interface NotificationProps {
  user: string;
  userId: string;
  type: string;
  notification: string;
  teamId?: string;
  date: string;
}
export interface Team {
  createdOn: string;
  description: string;
  members: {};
  name: string;
  ownerId: string;
  id?: string;
  ownerEmail: string;
}
