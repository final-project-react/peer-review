
export enum TaskButtonText {
  CREATE_ITEM = 'create item',
  TEAM_WORK_ITEMS = 'team work items',
  MY_WORK_ITEMS = 'my work items',
  MEMBERS = 'members',
}
