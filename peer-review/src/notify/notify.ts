import Notify from 'simple-notify';
import 'simple-notify/dist/simple-notify.min.css';

declare type notifyStatus = 'success' | 'warning' | 'error' | 'info';

export const pushNotify = (
  status: notifyStatus,
  title: string,
  text: string
) => {
  new Notify({
    status,
    title,
    text,
    effect: 'slide',
    speed: 300,
    showIcon: true,
    showCloseButton: true,
    autoclose: true,
    autotimeout: 5000,
    gap: 20,
    distance: 20,
    type: 1,
    position: 'top right',
  });
};
