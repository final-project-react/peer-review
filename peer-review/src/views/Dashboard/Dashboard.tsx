import { Box, Typography } from '@mui/material';
import React, { useState, useContext, useEffect } from 'react';
import SideBar from '../../components/SideBar/SideBar';
import { Outlet } from 'react-router-dom';
import { AuthContext } from '../../Context/Context';
import { auth, db } from '../../firebase/firebaseConfig';
import SideBarButtons from '../../components/SideBar/SideBarButtons';
import Home from '../Home/Home';
import { getUserData } from '../../services/user.services';
import { onValue, ref } from '@firebase/database';
import { NotificationProps } from '../../components/Interfaces/Interfaces';
import { Oval, TailSpin } from 'react-loader-spinner';
import { mainBackGroundColor } from '../../ui-helpers/ui-helpers';
// import { notificationsButton } from '../../components/SideBar/SideBar';

const Dashboard = () => {
  const { userData, setContext } = useContext(AuthContext);
  const [badgesFromNotif, setBadgesFromNotif] = useState<number>(0);
  const [useLoader, setUseLoader] = useState(false);

  const transferBadges = (badges: number) => {
    setBadgesFromNotif(badges);
  };

  useEffect(() => {
    let sub = true;

    const starCountRef = ref(db, 'notifications/' + userData?.uid);
    onValue(starCountRef, (snapshot) => {
      if (sub) {
        if (snapshot.val()) {
          const entriesNotification: [string, NotificationProps][] =
            Object.entries(snapshot.val());
          const notificationData: NotificationProps[] = entriesNotification.map(
            (not) => ({ notificationId: not[0], ...not[1] })
          );
          transferBadges(notificationData.length);
        }
      }

      return () => {
        sub = false;
      };
    });
  }, [userData?.uid]);

  console.log(useLoader);

  useEffect(() => {
    setUseLoader(true);
    auth.onAuthStateChanged((user) => {
      user &&
        getUserData(user.uid).then((snapshot) => {
          if (snapshot.exists()) {
            setContext({
              ...{
                user: auth.currentUser,
                userData: snapshot.val()[Object.keys(snapshot.val())[0]],
              },
            });
          }
        });
    });
    setTimeout(() => setUseLoader(false), 600);
  }, [setContext]);

  const homeOrDash = () => {
    if (!userData) {
      return <Home />;
    } else {
      return (
        <Box sx={{ display: 'flex', justifyContent: 'center' }}>
          <SideBar badgesFromNotif={badgesFromNotif} />
          <Outlet context={transferBadges} />
        </Box>
      );
    }
  };

  return (
    <>
      {useLoader ? (
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            width: '100vw',
            height: '100vh',
            background: mainBackGroundColor,
          }}
        >
          <Box>
            <Typography variant='h5' sx={{ margin: 4, ml: 9 }}>
              Loading please wait ...
            </Typography>
            <TailSpin
              ariaLabel='loading-indicator'
              height={400}
              width={400}
              color='blue'
            />
          </Box>
        </Box>
      ) : (
        homeOrDash()
      )}
    </>
  );
};

export default Dashboard;
