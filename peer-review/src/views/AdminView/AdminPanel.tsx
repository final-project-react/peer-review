import { TabContext } from '@mui/lab';
import { Container } from '@mui/material';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import React, { useContext, useState } from 'react';
import AllResearches from '../../components/Admin/AllResearches';
import AllTeams from '../../components/Admin/AllTeams';
import AllUsers from '../../components/Admin/AllUsers';
import PageLocation from '../../components/PageLocation/PageLocation';
import { AuthContext } from '../../Context/Context';
import { mainBackGroundColor } from '../../ui-helpers/ui-helpers';

const AdminPanel = () => {
  const { user, userData, setContext } = useContext(AuthContext); // kato moje bi shte trqbva s promise osven tyka da gi praq
  const [value, setValue] = useState(0);

  const handleChange = (event: any, newValue: number) => {
    setValue(newValue);
  };

  return (
    <Container
      maxWidth={false}
      style={{
        padding: 0,
        background: mainBackGroundColor,
        height: '100vh',
      }}
    >
      <PageLocation currentPage='Admin Panel'></PageLocation>
      <Container maxWidth='md'>
        <TabContext value={value.toString()}>
          <Box sx={{ width: '100%', marginTop: 14 }}>
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label='basic tabs example'
            >
              <Tab label='List Teams' />
              <Tab label='List Researches' />
              <Tab label='List Users' />
            </Tabs>
            {value === 0 && <AllTeams />}
            {value === 1 && <AllResearches />}
            {value === 2 && <AllUsers />}
          </Box>
        </TabContext>
      </Container>
    </Container>
  );
};

export default AdminPanel;
