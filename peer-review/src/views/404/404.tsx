import { Box } from '@mui/system';
import React from 'react';
import logo from '../../imageSrc/404.svg'
const NotFound = () => {
  return (
    <Box>
          <img src={logo} alt='home' style={{height: '100vh', width: '100vw'}}/>

    </Box>
  );
};

export default NotFound;
