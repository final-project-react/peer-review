import { Box, Grid, Typography } from '@mui/material';
import React, { useState, useEffect, useContext } from 'react';
import { useParams } from 'react-router-dom';
import { ResearchItem } from '../../components/Interfaces/Interfaces';
import Accepted from '../../components/MainComponents/Accepted/Accepted';
import Pending from '../../components/MainComponents/Pending/Pending';
import Rejected from '../../components/MainComponents/Rejected/Rejected';
import Requested from '../../components/MainComponents/Requested/Requested';
import { AuthContext } from '../../Context/Context';
import { getAllResearches } from '../../services/researchItem';
import { gridSpacing, mainBackGroundColor } from '../../ui-helpers/ui-helpers';


type Props = {
  changeContentItems: boolean;
  whichItem: string;
  dragAndDrop: {};
};

const WorkItemsTable = (props: Props) => {
  const [accepted, setAccepted] = useState<ResearchItem[]>([]);
  const [pending, setPending] = useState<ResearchItem[]>([]);
  const [rejected, setRejected] = useState<ResearchItem[]>([]);
  const [requested, setRequested] = useState<ResearchItem[]>([]);
  const { id } = useParams<string>();
  const { userData } = useContext(AuthContext);
  useEffect(() => {
    getAllResearches().then((researches) => {
      const researchedData: [string, ResearchItem][] = Object.entries(
        researches.val()
      );
      const filteredResearches = researchedData.reduce(
        (
          arrOfPosts: ResearchItem[],
          research: [string, ResearchItem]
        ) => {
          const idPost: string = research[0];
          const post: ResearchItem = research[1];
          if (props.changeContentItems) {
            if (userData?.uid === post.creator && id === post.teamId) {
              const finalPost = { ...post, id: idPost };
              return [...arrOfPosts, finalPost];
            }
          } else {
            if (id === post.teamId) {
              const finalPost = { ...post, id: idPost };
              return [...arrOfPosts, finalPost];
            }
          }
          return arrOfPosts;
        },
        []
      );
      setPending(
        filteredResearches.filter((research) => research.status === 'Pending')
      );
      setAccepted(
        filteredResearches.filter((research) => research.status === 'Accepted')
      );
      setRejected(
        filteredResearches.filter((research) => research.status === 'Rejected')
      );
      setRequested(
        filteredResearches.filter((research) => research.status === 'Requested')
      );
    });
  }, [props.changeContentItems, id, userData?.uid, props.dragAndDrop]);

  return (
    <Box>

      <Grid
        container
        spacing={gridSpacing}
        sx={{ padding: '30px', height: '86.5vh', background: mainBackGroundColor }}
      >
        <Accepted accepted={accepted} />
        <Pending pending={pending} />
        <Requested requested={requested} />
        <Rejected rejected={rejected} />
      </Grid>
    </Box>
  );
};

export default WorkItemsTable;
