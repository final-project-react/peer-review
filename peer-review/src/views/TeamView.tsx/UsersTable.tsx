import PersonRemoveIcon from '@mui/icons-material/PersonRemove';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import React, { useContext, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { AuthContext, UserData } from '../../Context/Context';
import {
  createNotification,
  getAllUsers,
  getUserData,
  removeTeamIdFromUser,
} from '../../services/user.services';
import { removeUserFromTeam } from '../../services/teams.services';
import { Box, Button, Tooltip } from '@mui/material';
import ModalDeleteUser from '../../components/ModalDeleteUser/ModalDeleteUser';

const ListUsers = ({
  renderOnNewMember,
  isOwner,
  setRenderOnNewMember,
}: any) => {
  const [filteredUsers, setFilteredUsers] = useState<[UserData][]>([]);
  const { id } = useParams<string>();
  const { userData } = useContext(AuthContext);
  useEffect(() => {
    getAllUsers().then((users) => {
      if (users.val()) {
        const usersData: [UserData][] = Object.values(users.val());
        const mappedUsers = usersData.filter((userArr: any) => {
          if (userArr.teams && id) {
            const arrayOfIdsUsers = Object.keys(userArr.teams);

            if (!arrayOfIdsUsers.includes(id)) {
              return false;
            }
          }
          if (!userArr.teams) return false;
          return true;
        });
        setFilteredUsers(mappedUsers);
      }
    });
  }, [id, renderOnNewMember]);

  const removeUser = (userId: any) => {
    if (id) {
      removeUserFromTeam(id, userId).then(() => {
        removeTeamIdFromUser(userId, id).then(() => {
          const newFilteredUsers = filteredUsers.filter(
            (user: any) => user.uid !== userId
          );
          setFilteredUsers(newFilteredUsers);
          setRenderOnNewMember();

          const data = {
            user: userData?.username,
            userId: userData?.uid,
            type: 'Team',
            notification: `You have been removed from  team. `,
            date: Date.now(),
          };
          createNotification(userId, data);
        });
      });
    }
    return true;
  };

  return (
    <Box sx={{ display: 'flex', justifyContent: 'center' }}>
      <TableContainer
        sx={{
          backgroundColor: 'white',
          borderRadius: '20px',
          height: '70vh',
          width: '75%',
          mt: 7,
        }}
      >
        <Table
          sx={{ minWidth: 650 }}
          stickyHeader
          size='medium'
          aria-label='a dense table'
        >
          <TableHead>
            <TableRow sx={{ backgroundColor: 'grey' }}>
              <TableCell sx={{ fontWeight: 'bold' }}>Users</TableCell>
              <TableCell sx={{ fontWeight: 'bold' }} align='center'>
                Email
              </TableCell>
              <TableCell sx={{ fontWeight: 'bold' }} align='center'>
                First name
              </TableCell>
              <TableCell sx={{ fontWeight: 'bold' }} align='center'>
                Last name
              </TableCell>
              {isOwner && (
                <TableCell sx={{ fontWeight: 'bold' }} align='center'>
                  Remove user
                </TableCell>
              )}
            </TableRow>
          </TableHead>
          <TableBody>
            {filteredUsers.map((user: any) => (
              <TableRow
                key={user.uid}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell component='th' scope='row'>
                  {user.username}
                </TableCell>
                <TableCell align='center'>{user.email}</TableCell>
                <TableCell align='center'>{user.firstName}</TableCell>
                <TableCell align='center'>{user.lastName}</TableCell>
                {isOwner && (
                  <TableCell align='center'>
                    {userData?.uid === user.uid ? (
                      <Button disabled>
                        <PersonRemoveIcon />
                      </Button>
                    ) : (
                      <ModalDeleteUser
                        removeUser={() => removeUser(user.uid)}
                      />
                    )}
                  </TableCell>
                )}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
};

export default ListUsers;
