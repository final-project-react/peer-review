import { Box, Button, ButtonGroup, TextField, Tooltip } from '@mui/material';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { pushNotify } from '../../notify/notify';
import {
  deleteTeamById,
  getTeamById,
  updateTeamUsers,
} from '../../services/teams.services';
import {
  findUserIdByEmail,
  updateUserTeam,
  getAllUsers,
  removeTeamIdFromUser,
  removeOwnerUserTeam,
  createNotification,
} from '../../services/user.services';
import FormCreateResearch from '../../components/FormCreateResearch/FormCreateResearch';
import UsersTable from './UsersTable';
import WorkItemsTable from './WorkItemsTable';
import { AuthContext } from '../../Context/Context';
import {
  deleteItem,
  getAllResearches,
  removeFromUserItem,
} from '../../services/researchItem';
import { ResearchItem } from '../../components/Interfaces/Interfaces';
import { removeItemComents } from '../../services/coments.services';
import { TaskButtonText } from '../../constants/team.const';
import PageLocation from '../../components/PageLocation/PageLocation';
import {
  mainBackGroundColor,
  mainColorForMainComponents,
} from '../../ui-helpers/ui-helpers';

import EngineeringIcon from '@mui/icons-material/Engineering';
import NoteAddIcon from '@mui/icons-material/NoteAdd';
import ViewComfyIcon from '@mui/icons-material/ViewComfy';
import GroupsIcon from '@mui/icons-material/Groups';

import PersonAddIcon from '@mui/icons-material/PersonAdd';
import ModalForDeleteTeam from '../../components/ModalForDeleteTeam/ModalForDeleteTeam';

const buttons = [
  <Tooltip title='Create work item'>
    <NoteAddIcon />
  </Tooltip>,
  <Tooltip title='Team work items'>
    <ViewComfyIcon />
  </Tooltip>,
  <Tooltip title='My work items'>
    <EngineeringIcon />
  </Tooltip>,
  <Tooltip title='Members'>
    <GroupsIcon />
  </Tooltip>,
];

const TeamView = ({ dragAndDrop }: any) => {
  const [formSearch, setFormSearch] = useState<string>('');
  const [currentTeam, setCurrentTeam] = useState<any>({});
  const [currentView, setCurrentView] = useState<string>(
    TaskButtonText.TEAM_WORK_ITEMS
  );
  const [renderOnNewMember, setRenderOnNewMember] = useState(false);
  const [isOwner, setIsOwner] = useState(false);
  const { id } = useParams<string>();
  const { userData } = useContext(AuthContext);
  const buttonsRef = useRef<Array<HTMLButtonElement>>([]);
  const navigate = useNavigate();

  useEffect(() => {
    buttonsRef.current.slice(0, Object.values(TaskButtonText).length);
    if (id) {
      getTeamById(id).then((team) => {
        const currTeam = team.val();
        setCurrentTeam(currTeam);
        if (currTeam.ownerId === userData?.uid) {
          setIsOwner(true);
        } else {
          setIsOwner(false);
        }
      });
    } else {
      pushNotify('error', 'Error', "team id doesn't exist");
    }
  }, [id, userData?.uid, renderOnNewMember]);

  // useEffect(() => {
  //   buttonsRef.current.slice(0, Object.values(TaskButtonText).length);
  // }, []);

  const deleteTeamOnClick = async () => {
    if (!isOwner) {
      return pushNotify(
        'info',
        "Sorry you can't do that",
        'Only owner of the team can delete!'
      );
    }
    if (id && userData?.uid) {
      const team = await getTeamById(id);
      const members = [...Object.keys(team.val().members), userData.uid];
      const allUsersData = await getAllUsers();
      const allUsers = Object.keys(allUsersData.val());
      const allUsersIdFromTeam = allUsers.filter((userId) =>
        members.includes(userId)
      );
      allUsersIdFromTeam.forEach((userId) => {
        const data = {
          user: userData?.username,
          userId: userData?.uid,
          type: 'Team',
          notification: `${userData.username} deleted team: ${currentTeam.name} you been part of it`,
          date: Date.now(),
        };
        removeTeamIdFromUser(userId, id);
        createNotification(userId, data);
      });
      await removeOwnerUserTeam(userData?.uid, id);

      const allItemsData = await getAllResearches();
      if (allItemsData.val()) {
        const allItems: [string, ResearchItem][] = Object.entries(
          allItemsData.val()
        );

        const allItemsFromTeam = allItems
          .filter((item) => item[1].teamId === id)
          .map((item) => {
            return { ...item[1], id: item[0] };
          });

        allItemsFromTeam.forEach((item) => {
          removeFromUserItem(item.creator, item.id);
          removeItemComents(item.id);
          deleteItem(item.id);
        });
      }
      deleteTeamById(id);
      navigate(-1);
    }
  };

  const addMember = async () => {
    if (formSearch === userData?.email) {
      return pushNotify('error', 'Error', 'you are already in the team');
    }
    const foundUser = await findUserIdByEmail(formSearch);
    if (!foundUser) {
      return pushNotify('error', 'Error', 'There is no such user');
    }

    if (foundUser && foundUser.uid && id && foundUser.email) {
      setRenderOnNewMember(!renderOnNewMember);
      await updateUserTeam(foundUser.uid, id);
      await updateTeamUsers(id, foundUser.uid, foundUser.email);
    } else {
      return pushNotify('error', 'Error', 'Something went wrong in TeamView');
    }
    setFormSearch('');
  };

  const onButtonClick = (text: TaskButtonText, indexOnClick: number) => {
    buttonsRef.current.forEach((button, i) => {
      if (i === indexOnClick) {
        button.style.background = '#1976d2';
        button.style.color = 'white';
      } else {
        button.style.background = 'none';
        button.style.color = '#1976d2';
      }
    });

    setCurrentView(text);
  };

  return (
    <Box
      sx={{ width: '100%', height: '100vh', background: mainBackGroundColor }}
    >
      <PageLocation currentPage={`  ${currentTeam.name}`} />
      <Box
        sx={{
          display: 'flex',
          width: '60%',
          justifyContent: 'space-between',
          ml: 4,
        }}
      >
        <Box sx={{ width: 1000, display: 'flex', flexDirection: 'row' }}>
          <TextField
            label='Add member email'
            type='email'
            name='email'
            variant='standard'
            value={formSearch}
            onChange={(e) => setFormSearch(e.target.value)}
            sx={{ height: '100%' }}
          />
          <Button variant='outlined' onClick={addMember} sx={{ fontSize: 14 }}>
            <Tooltip title='Add member'>
              <PersonAddIcon />
            </Tooltip>
          </Button>

          <ButtonGroup variant='outlined'>
            {Object.values(TaskButtonText).map((text, i) =>
              i === 1 ? (
                <Button
                  key={i}
                  ref={(el) => el && (buttonsRef.current[i] = el)}
                  sx={{
                    fontSize: 14,
                    width: 'auto',
                    '&:focus': { background: mainColorForMainComponents },
                    background: '#1976d2',
                    color: 'white',
                  }}
                  onClick={() => onButtonClick(text, i)}
                >
                  {buttons[i]}
                </Button>
              ) : (
                <Button
                  key={i}
                  ref={(el) => el && (buttonsRef.current[i] = el)}
                  sx={{
                    fontSize: 14,
                    width: 'auto',
                    '&:focus': { background: mainColorForMainComponents },
                  }}
                  onClick={() => onButtonClick(text, i)}
                >
                  {buttons[i]}
                </Button>
              )
            )}
            {isOwner && (
              <ModalForDeleteTeam
                deleteTeamOnClick={() => deleteTeamOnClick()}
              />
            )}
          </ButtonGroup>
        </Box>
      </Box>

      {currentView === TaskButtonText.MY_WORK_ITEMS && (
        <WorkItemsTable
          dragAndDrop={dragAndDrop}
          changeContentItems={true}
          whichItem={`this page is ${TaskButtonText.MY_WORK_ITEMS}`}
        />
      )}
      {currentView === TaskButtonText.TEAM_WORK_ITEMS && (
        <WorkItemsTable
          dragAndDrop={dragAndDrop}
          changeContentItems={false}
          whichItem={`this page is ${TaskButtonText.TEAM_WORK_ITEMS}`}
        />
      )}
      {currentView === TaskButtonText.MEMBERS && (
        <UsersTable
          renderOnNewMember={renderOnNewMember}
          setRenderOnNewMember={() => setRenderOnNewMember(!renderOnNewMember)}
          isOwner={isOwner}
        />
      )}
      {currentView === TaskButtonText.CREATE_ITEM && (
        <FormCreateResearch
          currentTeam={currentTeam}
          setCurrentView={(text: any, i: any) => onButtonClick(text, i)}
        />
      )}
    </Box>
  );
};

export default TeamView;
