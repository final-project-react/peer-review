import { Box, Grid } from '@mui/material';
import React from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';
import { useState } from 'react';
import { ResearchItem } from '../../../components/Interfaces/Interfaces';
import Accepted from '../../../components/MainComponents/Accepted/Accepted';
import Pending from '../../../components/MainComponents/Pending/Pending';
import Rejected from '../../../components/MainComponents/Rejected/Rejected';
import Requested from '../../../components/MainComponents/Requested/Requested';
import PageLocation from '../../../components/PageLocation/PageLocation';
import { AuthContext } from '../../../Context/Context';
import { getAllResearches } from '../../../services/researchItem';
import {
  gridSpacing,
  mainBackGroundColor,
} from '../../../ui-helpers/ui-helpers';

const MyResearch = ({ dragAndDrop }: any) => {
  const { userData } = useContext(AuthContext);
  const [accepted, setAccepted] = useState<ResearchItem[]>([]);
  const [pending, setPending] = useState<ResearchItem[]>([]);
  const [rejected, setRejected] = useState<ResearchItem[]>([]);
  const [requested, setRequested] = useState<ResearchItem[]>([]);

  useEffect(() => {
    getAllResearches().then((researches) => {
      const researchedData: [string, ResearchItem][] = Object.entries(
        researches.val()
      );
      const filteredResearches = researchedData.reduce(
        (acc: ResearchItem[], research: [string, ResearchItem]) => {
          const idPost: string = research[0];
          const post: ResearchItem = research[1];

          if (userData?.uid === post.creator) {
            const finalPost = { ...post, id: idPost };
            return [...acc, finalPost];
          }
          return acc;
        },
        []
      );

      setPending(
        filteredResearches.filter((research) => research.status === 'Pending')
      );
      setAccepted(
        filteredResearches.filter((research) => research.status === 'Accepted')
      );
      setRejected(
        filteredResearches.filter((research) => research.status === 'Rejected')
      );

      setRequested(
        filteredResearches.filter((research) => research.status === 'Requested')
      );
    });
  }, [userData?.uid, dragAndDrop]);

  // console.log(dragAndDrop);

  return (
    <Box sx={{ width: '100%', height: '100%' }}>
      <PageLocation currentPage={'My researches'} />
      <Grid
        container
        spacing={gridSpacing}
        sx={{
          padding: '60px',
          height: '92.8vh',
          backgroundColor: '#D9D9D9',
          background: mainBackGroundColor,
        }}
      >
        <Accepted accepted={accepted} />
        <Pending pending={pending} />
        <Requested requested={requested} />
        <Rejected rejected={rejected} />
      </Grid>
    </Box>
  );
};

export default MyResearch;
