import {
  Button,
  Chip,
  Grid,
  TextareaAutosize,
  TextField,
  Tooltip,
} from '@mui/material';
import { Box } from '@mui/system';
import React, { useContext, useRef, useState } from 'react';
import AddIcon from '@mui/icons-material/Add';
import { createTeam } from '../../../services/teams.services';
import { AuthContext, UserData } from '../../../Context/Context';
import {
  createNotification,
  findUserIdByEmail,
  getAllUsers,
  updateOwnerUserTeam,
  updateUserTeam,
} from '../../../services/user.services';
import { pushNotify } from '../../../notify/notify';
import { Link, NavLink, useNavigate, useSearchParams } from 'react-router-dom';
import PageLocation from '../../../components/PageLocation/PageLocation';
import { mainColorForMainComponents } from '../../../ui-helpers/ui-helpers';

const CreateTeam = () => {
  const { userData } = useContext(AuthContext);

  const [searchMembersForm, setSearchMembersForm] = useState('');
  const [members, setMembers] = useState<any>({});
  const [teamId, setTeamId] = useState<string | null>('');
  const hiddenFileInput = useRef<HTMLAnchorElement>(null);
  const [teamForm, setTeamForm] = useState<any>({
    name: '',
    description: '',
  });
  const navigate = useNavigate();

  const onAddMemberClick = () => {
    if (searchMembersForm === userData?.email) {
      return pushNotify('error', 'Error', 'you canot add yourself');
    }
    findUserIdByEmail(searchMembersForm).then((user?: UserData) => {
      if (user && searchMembersForm && user.uid) {
        const uid = user.uid;
        setMembers({ ...members, [uid]: user.email });
      } else {
        pushNotify('error', 'Error', 'There is no such user');
      }
    });
    setSearchMembersForm('');
  };
  console.log();

  const handleDelete = (memberToDelete: any) => () => {
    const newMembers = Object.entries<string>(members).reduce((obj, member) => {
      const uid: string = member[0];
      const email: string = member[1];
      if (member[1] !== memberToDelete) {
        return { ...obj, [uid]: email };
      }
      return obj;
    }, {});
    setMembers(newMembers);
  };

  const handleChange = (e: any) => {
    const { name, value } = e.target;
    setTeamForm({ ...teamForm, [name]: value });
  };
  const fillMembersForm = (e: any) => {
    const { value } = e.target;
    setSearchMembersForm(value);
  };

  const handleClick = () => {
    if (hiddenFileInput.current) {
      hiddenFileInput.current.click();
    }
  };

  const createTeamOnClick = () => {
    if (userData && userData.uid && userData.email) {
      if (!teamForm.name) {
        return pushNotify('error', 'Error', 'Team name canot be empty');
      }
      if (!members) {
        return pushNotify(
          'error',
          'Error',
          'You need to add at least one person to the team'
        );
      }

      createTeam(
        teamForm.name,
        teamForm.description,
        userData.uid,
        members,

        userData.email
      ).then((team) => {
        setTeamId(team.key);
        getAllUsers().then((users) => {
          if (team.key && userData.uid) {
            const key: string = team.key;

            const uid: string = userData.uid;
            updateOwnerUserTeam(uid, key);
            updateUserTeam(uid, key);

            Object.values(users.val()).forEach((user: any) => {
              if (user.uid in members && team.key) {
                updateUserTeam(user.uid, key);
                pushNotify('success', 'Success', 'You have created the team');
              }
            });
            Object.entries(members).forEach((member) => {
              const data = {
                user: userData?.username,
                userId: userData?.uid,
                type: 'Team',
                notification: `You been added to new team  with name: ${teamForm.name}`,
                teamId: team.key,
                date: Date.now(),
              };

              createNotification(member[0], data);

              handleClick();
            });
          }
        });
      });
    }
  };

  const createButtonFunc = () => {
    const membersNum: number = Object.keys(members).length;
    if (membersNum > 0 && teamForm.name) {
      return (
        <Button
          onClick={createTeamOnClick}
          sx={{
            width: '20%',
            color: 'white',
            backgroundColor: 'blue',
            textTransform: 'none',
            '&:hover': { backgroundColor: 'blue' },
            boxShadow: 'rgba(0, 0, 0, 0.35) 0px -50px 36px -28px inset',
          }}
        >
          CREATE
        </Button>
      );
    } else {
      return (
        <Button disabled sx={{ width: '20%', border: '1px solid' }}>
          CREATE
        </Button>
      );
    }
  };
  return (
    <Box
      sx={{
        width: '100%',
        height: '100vh',
        minWidth: 500,
        background: 'linear-gradient(to right, #eef2f3, #8e9eab)',
      }}
    >
      <PageLocation currentPage={'Create team'} />
      <Grid container sx={{ display: 'flex' }}>
        <Grid
          item
          xs={4}
          sx={{
            direction: 'row',
            ml: 2,
          }}
        ></Grid>

        <Grid
          container
          sx={{ height: '89vh', padding: '100px', paddingTop: '90px' }}
        >
          <Grid
            item
            xs={12}
            sx={{
              display: 'flex',
              justifyContent: 'center',
              flexDirection: 'column',
            }}
          >
            <TextField
              sx={{
                paddingBottom: '40px',
                width: '50%',
                display: 'flex',
                alignSelf: 'center',
              }}
              required
              label='Create team name'
              type='text'
              name='name'
              variant='standard'
              onChange={handleChange}
            />
            <TextareaAutosize
              style={{
                width: '50%',
                height: '23vh',
                border: '1px solid grey',
                backgroundColor: mainColorForMainComponents,
                display: 'flex',
                alignSelf: 'center',
                borderRadius: '5px',
              }}
              aria-label='Description (optional)'
              minRows={10}
              placeholder='Description (optional)'
              name='description'
              onChange={handleChange}
            />
          </Grid>

          <Grid
            item
            xs={12}
            sx={{
              display: 'flex',
              justifyContent: 'center',
              flexDirection: 'column',
            }}
          >
            <span
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'center',
              }}
            >
              <TextField
                sx={{
                  width: '45%',
                  display: 'flex',
                  alignSelf: 'center',
                  paddingBottom: '40px',
                }}
                label='Add Member'
                type='text'
                name='members'
                variant='standard'
                value={searchMembersForm}
                onChange={fillMembersForm}
              />
              <Tooltip title='Add a member'>
                <Button sx={{ color: 'blue' }} onClick={onAddMemberClick}>
                  <AddIcon
                    onClick={onAddMemberClick}
                    sx={{ fontSize: '34px', mt: '6px' }}
                  />
                </Button>
              </Tooltip>
            </span>

            <Box
              sx={{
                border: '1px solid grey',
                borderRadius: '5px',
                width: '50%',
                height: '23.5vh',
                backgroundColor: 'transparent',
                display: 'flex',
                alignSelf: 'center',
                flexWrap: 'wrap',
              }}
            >
              {members &&
                Object.values(members).map((member: any) => (
                  <Chip
                    variant='outlined'
                    sx={{ fontSize: 18, m: 1, fontWeight: 'bold' }}
                    key={member}
                    label={member}
                    onDelete={handleDelete(member)}
                  />
                ))}
            </Box>
          </Grid>
          <Grid
            item
            xs={12}
            sx={{
              display: 'flex',
              justifyContent: 'center',
              gap: '30px',
              height: '50px',
              marginTop: '30px',
            }}
          >
            {createButtonFunc()}
            <Button
              sx={{
                width: '20%',
                color: 'blue',
                backgroundColor: 'white',
                textTransform: 'none',
                '&:hover': { backgroundColor: 'white' },
                boxShadow: 'rgba(0, 0, 0, 0.35) 0px -50px 36px -28px inset',
              }}
            >
              CANCEL
            </Button>
          </Grid>
        </Grid>
      </Grid>
      <NavLink
        ref={hiddenFileInput}
        to={`/team/${teamId}`}
        style={{ display: 'hidden' }}
      ></NavLink>
    </Box>
  );
};

export default CreateTeam;
