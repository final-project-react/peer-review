import { updateEmail, updatePassword, User } from '@firebase/auth';
import {
  Avatar,
  Button,
  Grid,
  TextField,
  Tooltip,
  Typography,
} from '@mui/material';
import { NavLink } from 'react-router-dom';
import {
  getDownloadURL,
  ref as storageRef,
  uploadBytes,
} from 'firebase/storage';
import { Box, Container } from '@mui/system';
import React, { useContext, useRef, useState } from 'react';
import { AuthContext } from '../../../Context/Context';
import { auth } from '../../../firebase/firebaseConfig';
import { pushNotify } from '../../../notify/notify';
import {
  getUserData,
  updateAllUserPostAvatar,
  updateUserAvatar,
  updateUserEmail,
  updateUserUsername,
} from '../../../services/user.services';
import { storage } from '../../../firebase/firebaseConfig';
import { uid } from 'uid';
import Header from '../../../components/Header/Header';
import { mainFont } from '../../../ui-helpers/ui-helpers';
import { Oval } from 'react-loader-spinner';
import {
  getAllResearches,
  updateResearchAvatar,
  updateResearchAvatarAssignee,
} from '../../../services/researchItem';
import { ResearchItem } from '../../../components/Interfaces/Interfaces';

const changeViewsStyle = {
  display: 'flex',
  flexDirection: 'column',
  border: '1px solid grey',
  justifyContent: 'space-between',
  gap: '15px',
  height: '350px',
  borderRadius: '5%',
  padding: '40px',
};

const typographyTitleStyle = {
  alignSelf: 'center',
  fontFamily: mainFont,
};

const changeButtonStyle = {
  color: 'white',
  backgroundColor: 'blue',
  textTransform: 'none',
  '&:hover': { backgroundColor: 'blue' },
  boxShadow: 'none',
  marginBottom: '10px',
  marginTop: '10px',
  width: '70%',
  alignSelf: 'center',
};

const Profile = () => {
  const { user, userData, setContext } = useContext(AuthContext);
  const [newUsername, setNewUsername] = useState('');
  const [newEmail, setNewEmail] = useState('');
  const [newPass, setNewPass] = useState('');
  const hiddenFileInput = useRef<HTMLInputElement>(null);

  const updateEmailOnClick = () => {
    if (auth.currentUser && auth.currentUser.uid) {
      const uid: string = auth.currentUser.uid;
      updateEmail(auth.currentUser, newEmail)
        .then(() => {
          updateUserEmail(uid, newEmail).then(() => {
            const data = { ...userData, email: newEmail };
            setContext({
              ...{
                user: auth.currentUser,
                userData: data,
              },
            });
            pushNotify('success', 'Success', 'Your email has been changed');
          });
        })
        .catch((error) =>
          pushNotify('error', 'Error', `${console.log(error.message)}`)
        );
    } else {
      pushNotify('error', 'Error', "Something wen't wrong with authentication");
    }
  };
  const updatePassOnClick = () => {
    if (auth.currentUser && auth) {
      const user: User = auth.currentUser;
      const newPassword: string = newPass;
      updatePassword(user, newPassword)
        .then(() => {
          pushNotify('success', 'Success', 'Password has been changed');
        })
        .catch((error) =>
          pushNotify('error', 'Error', `${console.log(error.message)}`)
        );
    }
  };
  const updateUsernameOnClick = () => {
    if (auth?.currentUser?.uid) {
      const uid: string = auth.currentUser.uid;
      const newUsernameT: string = newUsername;
      updateUserUsername(uid, newUsernameT)
        .then(() => {
          const data = { ...userData, username: newUsernameT };
          setContext({
            ...{
              user: auth.currentUser,
              userData: data,
            },
          });
          pushNotify(
            'success',
            'Success',
            `Your new username is ${newUsername}`
          );
        })
        .catch((error) =>
          pushNotify('error', 'Error', `${console.log(error.message)}`)
        );
    }
  };

  const handleClick = (event: any) => {
    if (hiddenFileInput.current) {
      hiddenFileInput.current.click();
    }
  };

  const handleChange = (e: any) => {
    const file = e.target.files[0];
    if (!file) return alert('Please select a file!');

    const picture = storageRef(
      storage,
      `avatars/${userData && userData?.username}/${uid()}`
    );

    uploadBytes(picture, file).then((snapshot) => {
      return getDownloadURL(snapshot.ref)
        .then((url) => {
          if (url && userData && userData?.uid) {
            updateUserAvatar(userData.uid, url);
            getAllResearches().then((data) => {
              if (data.val()) {
                const entrResearches: [string, ResearchItem][] = Object.entries(
                  data.val()
                );

                entrResearches.forEach((research) => {
                  if (research[1].assignee === userData.email) {
                    updateResearchAvatarAssignee(research[0], url);
                  }
                });
              }
            });
          }
        })
        .then(() => {
          if (userData && userData.uid && userData.email) {
            getUserData(userData.uid).then((result) => {
              if (result && userData && userData.uid) {
                const research: any[] = Object.values(result.val());
                const researchId: string = research[0].avatar;
                updateAllUserPostAvatar(userData.uid, researchId);
              }
              setContext({
                user,
                userData: Object.values(result.val())[0],
              });
            });
          }
        })
        .catch((e) => console.log(e.message));
    });
  };
  return (
    <Box
      sx={{
        width: '100%',
        height: '100%',
        minWidth: 500,
        background: 'linear-gradient(to right, #eef2f3, #8e9eab)',
      }}
    >
      <Header page={'My profile'} />
      <Container maxWidth='lg' sx={{ height: '79.6vh' }}>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            borderTop: '1px solid grey',
            borderBottom: '1px solid grey',
            marginTop: '130px',
            paddingTop: '15px',
            paddingBottom: '6px',
          }}
        >
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'row',
            }}
          >
            <div onClick={handleClick}>
              {userData ? (
                <Tooltip title='Change profile'>
                  <Avatar
                    variant='rounded'
                    key={Date.now()}
                    src={
                      userData.avatar ? userData.avatar : '/broken-image.jpg'
                    }
                    sx={{ width: '100px', height: '100px', cursor: 'pointer' }}
                  />
                </Tooltip>
              ) : (
                <Oval
                  ariaLabel='loading-indicator'
                  height={100}
                  width={100}
                  strokeWidth={5}
                  strokeWidthSecondary={1}
                  color='blue'
                  secondaryColor='white'
                />
              )}
            </div>

            <Box sx={{ display: 'flex', flexDirection: 'column-reverse' }}>
              <Typography
                variant='h5'
                sx={{ paddingLeft: '10px', fontFamily: mainFont }}
              >
                Username: {userData?.username}
              </Typography>
              {userData?.userRole === 'admin' && (
                <Button
                  sx={{ '&:focus': { background: 'rgba(244, 245, 247, 1)' } }}
                >
                  <NavLink style={{ textDecoration: 'none' }} to='admin-panel'>
                    Admin Panel
                  </NavLink>
                </Button>
              )}
            </Box>
            <input
              style={{ display: 'none' }}
              onChange={handleChange}
              ref={hiddenFileInput}
              type='file'
              id='fileInput'
              name='attachement'
            />
          </Box>

          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            <Typography variant='h5' sx={{ fontFamily: mainFont }}>
              First name: {userData?.firstName}
            </Typography>
            <Typography variant='h5' sx={{ fontFamily: mainFont }}>
              {' '}
              Last name: {userData?.lastName}
            </Typography>
            <Typography variant='h5' sx={{ fontFamily: mainFont }}>
              {' '}
              Email: {userData?.email}
            </Typography>
          </Box>
        </Box>

        {/* buttons for posts */}

        <Grid
          container
          sx={{
            display: 'flex',
            justifyContent: 'space-between',
            mt: 16,
          }}
        >
          <Grid item xs={3.8} sx={changeViewsStyle}>
            <Typography variant='h5' sx={typographyTitleStyle}>
              Change Username
            </Typography>

            <TextField
              label='New username'
              type='text'
              name='username'
              variant='standard'
              onChange={(e) => setNewUsername(e.target.value)}
            />
            <Button onClick={updateUsernameOnClick} sx={changeButtonStyle}>
              Change
            </Button>
          </Grid>
          <Grid item xs={3.8} sx={changeViewsStyle}>
            <Typography variant='h5' sx={typographyTitleStyle}>
              Change Password
            </Typography>

            <TextField
              label='New password'
              type='password'
              name='password'
              variant='standard'
              onChange={(e) => setNewPass(e.target.value)}
            />

            <Button
              onClick={updatePassOnClick}
              variant='outlined'
              sx={changeButtonStyle}
            >
              Change
            </Button>
          </Grid>

          <Grid item xs={3.8} sx={changeViewsStyle}>
            <Typography variant='h5' sx={typographyTitleStyle}>
              Change Email
            </Typography>

            <TextField
              label='New email'
              type='text'
              name='email'
              variant='standard'
              onChange={(e) => setNewEmail(e.target.value)}
            />
            <Button onClick={updateEmailOnClick} sx={changeButtonStyle}>
              Change
            </Button>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default Profile;
