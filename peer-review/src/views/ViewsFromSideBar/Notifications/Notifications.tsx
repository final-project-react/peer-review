import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Typography,
  Tooltip,
} from '@mui/material';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import React, { useContext, useEffect, useState } from 'react';
import { uid } from 'uid';
import { NotificationProps } from '../../../components/Interfaces/Interfaces';
import PageLocation from '../../../components/PageLocation/PageLocation';
import { AuthContext } from '../../../Context/Context';
import {
  deleteNotification,
  updateUserBadges,
} from '../../../services/user.services';
import { ref, onValue } from 'firebase/database';
import { db } from '../../../firebase/firebaseConfig';
import { useOutletContext } from 'react-router-dom';
import { mainBackGroundColor, mainFont } from '../../../ui-helpers/ui-helpers';
import Moment from 'react-moment';


const Notification = ({ notification, handleLiveTrack, index}: any) => {
  const { user, userData, setContext } = useContext(AuthContext);
  const handleDeleteNotification = () => {
    deleteNotification(userData?.uid, notification.notificationId);
    if (userData && userData.uid) {
      const newBadgeNumber = +userData.badges - 1;
      const data = { ...userData, badges: newBadgeNumber };
      setContext({ user, userData: data });
      updateUserBadges(userData?.uid, newBadgeNumber);
    }
    return handleLiveTrack();
  };

 

  const colorForCard = index % 2 === 0 ? '#D9D9D9': 'white' ;

  return (
    <Card
      sx={{
        marginLeft: '30%',
        marginRight: '30%',
        marginBottom: '15px',
        marginTop: '15px',
        backgroundColor: colorForCard,
        borderRadius: '10px',
        outline: 'none',
        boxShadow: 'none',
        border: '1px solid #B2B1B1',
      }}
    >
      <CardHeader
        title={`New notification`}
        action={
          <Tooltip title='Delete notification'>
            <Button onClick={handleDeleteNotification}>
              <DeleteForeverIcon />
            </Button>
          </Tooltip>
        }
        subheader={notification.date && < Moment fromNow>{notification.date}</Moment>}
      />

      <CardContent sx={{ paddingLeft: '25px' }}>
        <Typography variant='h6'>{notification.notification}</Typography>
      </CardContent>
    </Card>
  );
};

const Notifications = () => {
  const { userData } = useContext(AuthContext);
  const [notifications, setNotifications] = useState<any[]>([]);
  const [liveTrack, setLiveTrack] = useState(true);

  const transferBadges: (badges: number) => void = useOutletContext();
  const handleLiveTrack = () => {
    setLiveTrack(!liveTrack);
  };

  useEffect(() => {
    let sub = true;

    const starCountRef = ref(db, 'notifications/' + userData?.uid);
    onValue(starCountRef, (snapshot) => {
      if (snapshot.val() === null) {
        transferBadges(0);
        return setNotifications([]);
      }

      if (sub && snapshot.val() !== null) {
        const entriesNotification: [string, NotificationProps][] =
          Object.entries(snapshot.val());
        const notificationData: NotificationProps[] = entriesNotification.map(
          (not) => ({ notificationId: not[0], ...not[1] })
        );

        setNotifications(notificationData);
        transferBadges(notificationData.length);
      }
      return () => {
        sub = false;
      };
    });
  }, [userData?.uid, transferBadges]);

  return (
    <Box
      sx={{ width: '100%', height: '100vh', background: mainBackGroundColor }}
    >
      <PageLocation currentPage={'Notifications'} />
      <Box
        sx={{
          marginTop: '50px',
          maxHeight: '80vh',
          overflowY: 'auto',
          borderRadius: '5%',
        }}
      >
        {notifications.length === 0 ? (
          <Typography
            variant='h5'
            sx={{ display: 'flex', justifyContent: 'center' }}
          >
            {' '}
            You don't have any notifications
          </Typography>
        ) : (
          notifications.map((notification, index) => (
            <Notification
              key={uid()}
              notification={notification}
              handleLiveTrack={handleLiveTrack}
              index={index}
            />
          ))
        )}
      </Box>
    </Box>
  );
};

export default Notifications;
