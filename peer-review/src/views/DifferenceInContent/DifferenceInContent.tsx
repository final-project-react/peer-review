import * as React from 'react';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Button from '@mui/material/Button';
import DifferenceIcon from '@mui/icons-material/Difference';
import { Quill } from 'react-quill';
import { useCallback } from 'react';
import { useState } from 'react';
import { Tooltip } from '@mui/material';


const Delta = Quill.import('delta');

const style = {
  position: 'absolute' as 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '40vw',
  height: '78vh',
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
  overflowX :'auto',
};
const modules = [
  [{ header: [1, 2, 3, 4, 5, 6, false] }],
  [{ font: [] }],
  [{ size: [] }],
  ["bold", "italic", "underline", "strike", "blockquote"],
  [
    { list: "ordered" },
    { list: "bullet" },
    { indent: "-1" },
    { indent: "+1" }
  ],
  ["link", "image", "video"],
  ["clean", "code-block"]
];

const textDifference = {
  color: 'white',
  backgroundColor:'blue',
  "&:hover": { backgroundColor: "blue"},
  marginTop: '10px',
  minWidth: '30px',
  marginRight: '15px',
  width: '33px'
}


const DifferenceInContent = (contents: any) => {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(true)
    findDiff(contents.oldDataContent, contents.newDataContent);
  };
  const handleClose = () => setOpen(false);

  const [quill, setQuill] = useState< any | null>(null)
  
  const wrapperRef = useCallback((wrapper: HTMLElement) => {
    
    if (wrapper === null) return;
    
    wrapper.innerHTML = '';
    const editor = document.createElement('Box');
    wrapper.append(editor);
    const newEditor = new Quill(editor, {theme: 'snow', modules: { toolbar: modules }, readOnly: true})
    setQuill(newEditor)
  }, []);
  

  
  const findDiff = (oldContentData: {ops: any[]}, newContentData: {ops: any[]}) => {
    
    const oldContent = new Delta(oldContentData);
    const newContent = new Delta(newContentData);

    const diff = oldContent.diff(newContent);

    for (var i = 0; i < diff.ops.length; i++) {
      const op = diff.ops[i];

      if (op.hasOwnProperty('insert')) {

        op.attributes = {
          background: "#cce8cc",
          color: "#003700"
        };
      }

      if (op.hasOwnProperty('delete')) {
        // keep the text
        op.retain = op.delete;
        delete op.delete;

        op.attributes = {
          background: "#e8cccc",
          color: "#370000",
          strike: true
        };
      }
    }

    const adjusted = oldContent.compose(diff);
    quill.setContents(adjusted);
  }

  
  return (
    <div>
      <Tooltip title='Show differences'>

        <Button sx={textDifference} variant='outlined' onClick={handleOpen}><DifferenceIcon/> </Button>
      </Tooltip>
      <Modal
        keepMounted
        open={open}
        onClose={handleClose}
      >
        <Box sx={style}>
        <Box id='container-editor' ref={wrapperRef}></Box>
        </Box>
      </Modal>
    </div>
  );
}

export default DifferenceInContent;


