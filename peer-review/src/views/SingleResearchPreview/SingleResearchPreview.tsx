import React from 'react'
import Editor from './Editor'
import Box from '@mui/material/Box';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import { useState } from 'react';
import { Button, Grid } from '@mui/material';
import { useParams } from 'react-router-dom';
import { useEffect } from 'react';
import { addComment, deleteComment, getAllCommentsByResearchItemId, getResearchById } from '../../services/researchItem';

import CommentCard from '../../components/CommentCard/CommentCard';
import Header from '../../components/Header/Header';
import InReview from './InReview';
import OnlyViewEditor from './OnlyViewEditor';
import { useContext } from 'react';
import { AuthContext } from '../../Context/Context';
import { createNotification, findUserIdByEmail } from '../../services/user.services';
import { getCommentById } from '../../services/coments.services';
import { ResearchItem } from '../../components/Interfaces/Interfaces';
import PageLocation from '../../components/PageLocation/PageLocation';

const buttonPreview = {
  color: 'white',
  backgroundColor:'blue',
  textTransform: 'none', 
  "&:hover": { backgroundColor: "blue"},
  boxShadow: 'none',
  marginBottom: '10px',
  marginTop: '10px',

}


const SingleResearchPreview = () => {
  const {userData} = useContext(AuthContext);
  const {id} = useParams();
  const [researchItem, setResearchItem] = useState<ResearchItem>();
  const [researchItemId, setResearchItemId] = useState<string | null>();
  const [openReview, setOpenReview] = useState(true);
  const [commentsView, setCommentsView] = useState(true);
  const [comment, setComment] = useState<string>();
  const [allComments, setAllComments] = useState< any >();
  const [hasAddedComment, setHasAddedComment] = useState(false);
  
  const closeSection = () => setOpenReview(!openReview);
  const comments = () => setCommentsView(!commentsView)

  const handleComment = () => {
    const commentData = {
      content: comment,
      username: userData?.username,
      creatorId: userData?.uid,
      createdOn: Date.now(),
    }
    addComment(researchItemId, commentData)
      .then(() => getAllCommentsByResearchItemId(researchItemId)
      .then(comments => setAllComments(comments.val())))
      .then(() => setComment(''))
      .then(() => setHasAddedComment(true));
      
      const data = {
        user: userData?.username,
        userId: userData?.uid,
        type: 'Work Item',
        notification: `${userData?.username} comment your Research in team: ${researchItem?.team}`,
        teamId: researchItem?.teamId,
        date: Date.now(),
      }

      createNotification(researchItem?.creator, data);
      const assignee = researchItem?.assignee;
      return findUserIdByEmail(assignee).then(user => createNotification(user?.uid, data));
  }

  const handleDeleteComment = (commentId: string) => {
    if (researchItem?.creator !== userData?.uid) return
    const data = {
      user: userData?.username,
      userId: userData?.uid,
      type: 'Work Item',
      notification: `${researchItem?.reporter} resolve your suggestion in team: ${researchItem?.team}`,
      teamId: researchItem?.teamId,
      date: Date.now(),
    }

    getCommentById(researchItemId, commentId).then(res => createNotification(res.val().creatorId, data));

    return deleteComment(researchItemId, commentId)
      .then(() => getAllCommentsByResearchItemId(researchItemId)
      .then(comments => setAllComments(comments.val())));
   

  }

  useEffect(() => {
      getResearchById(id).then(item => {
        setResearchItem(item.val());
        setResearchItemId(item.key);
      }).then(() => getAllCommentsByResearchItemId(researchItemId)
        .then(comments => setAllComments(comments.val())));
  }, [id, researchItemId]);


  return (
    <Box sx={{background: 'linear-gradient(to right, #eef2f3, #8e9eab)'}}>
      <Box >
        <PageLocation currentPage={`${researchItem?.title}`} />
      </Box>
      <Grid container sx={{padding: '63px'}}>
        <Grid item xs={7} sx={{display:'flex', justifyContent: 'flex-end', paddingRight: '10px'}}>
        <Box>
          { 
          researchItem?.assignee === userData?.email 
          ? <OnlyViewEditor researchItem={researchItem}/> 

          : 

          ((userData?.uid === researchItem?.creator && researchItem?.status === 'Requested') || (userData?.uid === researchItem?.creator && researchItem?.status === 'Pending')
          ? <Editor researchItem={researchItem} itemId={researchItemId} />
          : <OnlyViewEditor researchItem={researchItem}/>) 
          }
        </Box>

        </Grid>


        <Grid item xs={5} sx={{display: 'flex', flexDirection: 'row'}}>


        {commentsView && 
          <Box  
            sx={{
              width: '30px',
              display: 'flex',
              flexDirection: 'row',
              background:'white',
              borderRadius: '10px',
              height: '78.5vh'
            }}
            
          >
            <span>
              <ChevronRightIcon onClick={comments}  sx={{paddingTop: '8px'}} />  
              <h4 style={{writingMode: 'vertical-lr', margin: 'auto'}}>Open Comments</h4>
            </span>
            </Box>
        }
            {!commentsView &&
            <Box sx={{height: '60vh'}}>
              <span>
                <Box onClick={comments} sx={{display: 'flex', flexDirection: 'row', paddingLeft: '10px', cursor: 'pointer'}}>
                  <span style={{display: 'flex', flexDirection: 'row'}}>
                    <h4 style={{margin: 'auto'}}>
                      Close
                    </h4>
                    <KeyboardArrowDownIcon sx={{display: 'flex', flexDirection: 'column-reverse'}} />
                  </span>
                </Box>
                <Box sx={{width: '20vw',border: '1px solid #B2B1B1', borderRadius: '10px', backgroundColor: '#F4F5F7',}}>
                <Box sx={{height: '59vh', position: 'relative', overflow: 'auto'}}>
                  {allComments && Object.entries(allComments).map(each => <CommentCard key={each[0]} comment={each} handleDeleteComment={handleDeleteComment}/>)}
                </Box>

                <Box sx={{display: 'flex', flexDirection: 'column' ,alignItems:'center', borderTop: '3px solid black'}}>
                    <textarea 
                      placeholder='Comment section' 
                      style={{width: '18vw', height: '10vh', border:'none', marginTop: '10px'}}
                      value={comment}
                      onChange={(e) => setComment(e.target.value)}
                      ></textarea>
                    <Button 
                      variant='contained'
                      sx={buttonPreview}
                      onClick={handleComment}
                    >
                      Add your comment
                    </Button>
                  </Box>
                  
                </Box>
              </span>
            </Box>}

        {openReview &&
             <Box 
              sx={{
                width: '30px',
                display: 'flex',
                flexDirection: 'row',
                marginLeft: '8px',
                marginRight: '8px',
                background:'white',
                borderRadius: '10px',
                height: '78.5vh',
              }}>
                <span>
                  <ChevronRightIcon onClick={closeSection} sx={{paddingTop: '8px'}} />
                  <h4 style={{writingMode: 'vertical-lr', margin: 'auto'}}>Open Details</h4>
                </span>
             </Box>
        }

          {!openReview &&  
            <span >
              <Box onClick={closeSection} sx={{display: 'flex', flexDirection: 'row', paddingLeft: '10px', cursor: 'pointer'}}>
              <span style={{display: 'flex', flexDirection: 'row'}}>
                  <h4 style={{margin: 'auto'}}>
                    Close
                  </h4>
                  <KeyboardArrowDownIcon sx={{display: 'flex', flexDirection: 'column-reverse'}} />
                </span>

              </Box>
              <InReview researchItem={researchItem} hasAddedComment={hasAddedComment}/>
            </span> 
          }

        </Grid>
      </Grid>
      
    </Box>

  )
}

export default SingleResearchPreview
