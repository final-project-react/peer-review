import { Box, Button, Chip, Link, Tooltip } from "@mui/material";
import EditIcon from '@mui/icons-material/Edit';
import AttachFileIcon from '@mui/icons-material/AttachFile';
import DifferenceIcon from '@mui/icons-material/Difference';
import { useCallback, useEffect } from "react";
import { useState } from "react";
import { Quill } from "react-quill";
import 'react-quill/dist/quill.snow.css';

import { pushNotify } from "../../notify/notify";
import { changeContentResearchItem, oldContent } from "../../services/researchItem";
import { createNotification, findUserIdByEmail } from "../../services/user.services";

import DifferenceInContent from "../DifferenceInContent/DifferenceInContent";
import './editor.css';

const modules = [
  [{ header: [1, 2, 3, 4, 5, 6, false] }],
  [{ font: [] }],
  [{ size: [] }],
  ["bold", "italic", "underline", "strike", "blockquote"],
  [
    { list: "ordered" },
    { list: "bullet" },
    { indent: "-1" },
    { indent: "+1" }
  ],
  ["link", "image", "video"],
  ["clean", "code-block"]
];

const ButtonEdit = {
  color: 'white',
  backgroundColor:'blue',
  "&:hover": { backgroundColor: "blue"},
  marginBottom: '10px',
  minWidth: '30px',
  marginRight: '15px',

}
const ButtonDownload = {
  color: 'white',
  backgroundColor:'blue',
  "&:hover": { backgroundColor: "blue"},
  minWidth: '30px',
  marginRight: '15px',
  width: '33px'
}

const Editor = ({researchItem, itemId}: any) => {

  const [quill, setQuill] = useState< any | null>(null);
  useEffect(() => {
    quill?.setContents(researchItem?.content)

  }, [researchItem,quill])


  const handleContentChange = () => {
    const data = {
      user: researchItem?.reporter,
      userId: researchItem?.creator,
      type: 'Work Item',
      notification: `${researchItem?.reporter} edit his work item in team: ${researchItem?.team}`,
      teamId: researchItem?.teamId,
      date: Date.now(),
    }

    findUserIdByEmail(researchItem.assignee).then(res => createNotification(res?.uid, data));

    return oldContent(itemId, researchItem.content)
      .then(() => changeContentResearchItem(itemId, quill.getContents())
      .then(() => pushNotify('success', 'Success', 'You edit is successfully')))
    
  }
  
  const wrapperRef = useCallback((wrapper: HTMLElement) => {
    
    if (wrapper === null) return;
    
    wrapper.innerHTML = '';
    const editor = document.createElement('Box');
    wrapper.append(editor);
    const newEditor = new Quill(editor, {theme: 'snow', modules: { toolbar: modules }})
    setQuill(newEditor)
  }, []);
  
  return (
    <Box sx={{display: 'flex'}}>
      <Box sx={{display: 'flex', flexDirection: 'column', width: '50px'}}>
      <Tooltip title='Publish changes'>

        <Button
              variant='outlined'
              sx={ButtonEdit}
              onClick={handleContentChange}
        >
          <EditIcon/>
        </Button>
      </Tooltip>
        {researchItem?.file && <Tooltip title='Download file'><Button sx={ButtonDownload} href={researchItem?.file}><AttachFileIcon /></Button></Tooltip>}
        {researchItem?.oldContent && < DifferenceInContent oldDataContent={researchItem?.oldContent} newDataContent={researchItem?.content}/>}
      </Box>
      <Box id='editor-review'>


        <Box id='container-editor-review' ref={wrapperRef}></Box>
      </Box>
    </Box>
  )
}

export default Editor;