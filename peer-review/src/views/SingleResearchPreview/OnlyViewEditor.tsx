import { Box, Button, Tooltip } from '@mui/material';
import React from 'react'
import { useCallback } from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { Quill } from "react-quill";
import 'react-quill/dist/quill.bubble.css';
import DifferenceInContent from '../DifferenceInContent/DifferenceInContent';
import AttachFileIcon from '@mui/icons-material/AttachFile';
import './onlyViewEditor.css'

const ButtonDownload = {
  color: 'white',
  backgroundColor:'blue',
  "&:hover": { backgroundColor: "blue"},
  minWidth: '30px',
  marginRight: '15px',
  width: '33px'
}


const OnlyViewEditor = ({researchItem}: any) => {

  const [quill, setQuill] = useState< any | null>(null);
  
  useEffect(() => {
    quill?.setContents(researchItem?.content)

  }, [researchItem,quill])
  
  const wrapperRef = useCallback((wrapper: HTMLElement) => {
    
    if (wrapper === null) return;
    
    wrapper.innerHTML = '';
    const editor = document.createElement('Box');
    wrapper.append(editor);
    const newEditor = new Quill(editor, {theme: 'snow', readOnly: true})
    setQuill(newEditor)
  }, []);
  
  return (
    <Box sx={{display: 'flex'}}>
      <Box sx={{display: 'flex', flexDirection: 'column', width: '50px'}}>

      {researchItem?.file && <Tooltip title='Download file'><Button sx={ButtonDownload} href={researchItem?.file}><AttachFileIcon /></Button></Tooltip>}
      {researchItem?.oldContent && < DifferenceInContent oldDataContent={researchItem?.oldContent} newDataContent={researchItem?.content}/>}
      </Box>
      <Box id='editor-read-only'>
        <Box  id='only-review-editor' ref={wrapperRef}></Box>
      </Box>
    </Box>
  )
}

export default OnlyViewEditor
