import { Box, Button, MenuItem, TextField, Typography } from '@mui/material'
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment'
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'
import React, { ChangeEvent, useState } from 'react'
import { useContext } from 'react'
import { useParams } from 'react-router-dom'
import { AuthContext } from '../../Context/Context'
import { pushNotify } from '../../notify/notify'
import { changeItemDueDate, changeItemStatus } from '../../services/researchItem'
import { createNotification } from '../../services/user.services'
import { mainFont } from '../../ui-helpers/ui-helpers'

const boxStyle = {
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-evenly',
  padding: '10px',
}

const textFieldSize = {
  width: '150px',
}
const titleSize = {
  width: '80px',
  fontFamily: mainFont,
  fontWeight: 'bold',
  color: '#black', 
  borderBottom: ' 1px double #B2B1B1',
  display: 'flex',
  alignSelf: 'flex-end'
}


const status = [
  {
    value: 'Pending',
    label: 'Pending',
  },
  {
    value: 'Accepted',
    label: 'Accept',
  },
  {
    value: 'Requested',
    label: 'Request Changes',
  },
  {
    value: 'Rejected',
    label: 'Reject',
  },
]

const updateAssignee = {
  color: 'white',
  backgroundColor:'blue',
  textTransform: 'none', 
  "&:hover": { backgroundColor: "blue"},
  boxShadow: 'none',
  marginBottom: '10px',
  marginTop: '10px',
  width: '50%',
  display:'flex',
  alignSelf: 'center',

}

const InReview = ({researchItem, hasAddedComment}: any) => {

  const { userData } = useContext(AuthContext);
  const { id } = useParams();
  const [newStatus, setNewStatus] = useState<string>(researchItem?.status);
  const [newDueDate, setNewDueDate] = useState<string>(researchItem?.dueDate)
  const handleStatusChange = (e: ChangeEvent<HTMLInputElement>) => {
    setNewStatus(e.target.value);
  }

  const updateNewStatus = () => {
    if (newStatus === 'Rejected' && !hasAddedComment) return pushNotify('error','Error', 'Please provide a rejection reason !')
    changeItemDueDate(id, newDueDate);
    changeItemStatus(id, newStatus)
    .then(() => pushNotify('success', 'Success', 'Status updated Successfully'))
    
    const data = {
      user: userData?.username,
      userId: userData?.uid,
      type: 'Work Item',
      notification: `Work item status been changed in team: ${researchItem?.team}`,
      teamId: researchItem?.teamId,
      date: Date.now(),
    }

    return createNotification(researchItem?.creator, data);
  }

  const handleNewDueDate = (e :any) => {
    if (new Date(e.target.value) <= new Date(researchItem?.dueDate)) return pushNotify('error', 'Error', 'Cannot choose date in the past.')
    setNewDueDate(e.target.value)
  }


  return (
    <Box 
      sx={{
        width: '20vw',
        paddingTop: '20px',
        marginLeft:'10px',
        marginRight: '10px',
        border: '1px solid #B2B1B1',
        borderRadius: '10px',
        backgroundColor: '#F4F5F7',
        height: '74.7vh',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-evenly'
      }}
    >

          <Typography variant='h4' sx={{display: 'flex', justifyContent: 'center', paddingBottom: '10px'}}>
            {researchItem?.status}
          </Typography>



      <Box sx={boxStyle}>
      <Typography sx={titleSize}>
        Assigned
        </Typography>
        <TextField
          variant='standard'
          value={researchItem?.assignee}
          sx={textFieldSize}
        />
      </Box>


      <Box sx={boxStyle}>
        <Typography sx={titleSize}>
        Creator
        </Typography>
        <TextField
          variant='standard'
          value={researchItem?.reporter}
          sx={textFieldSize}
        />
      </Box>

{/* 
      <Box sx={boxStyle}>

        <Typography sx={titleSize}>

        Reporter
        </Typography>
        <TextField
          variant='standard'
          value={researchItem?.reporter}
          sx={textFieldSize}
        />
      </Box> */}


      <Box sx={boxStyle}>

        <Typography sx={titleSize}>

        Team
        </Typography>
        <TextField
          variant='standard'
          value={researchItem?.team}
          sx={textFieldSize}
        />
      </Box>


      <Box sx={boxStyle}>

        <Typography sx={titleSize}>
        Status
        </Typography>
        {(researchItem?.status === 'Pending' && researchItem?.assignee === userData?.email) 
        || 
        (researchItem?.status === 'Requested' && researchItem?.creator === userData?.uid)
        ?
        <TextField
            variant='standard'
            select
            required
            onChange={handleStatusChange}
            value={newStatus}
            label='Status'
            sx={textFieldSize}
        >
            {status.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}

        </TextField> 

        : <TextField
          value={researchItem?.status}
          sx={textFieldSize}
          variant='standard'
          />
        }
      </Box>

      <Box sx={boxStyle}>

        <Typography sx={titleSize}>
        Priority
        </Typography>
        <TextField 
          variant='standard'
          value={researchItem?.priority}
          sx={textFieldSize}
        />
      </Box>

      <Box sx={boxStyle}>

        <Typography sx={titleSize}>

          Due date
        </Typography>
        {/* <TextField 
          variant='standard'
          value={researchItem?.dueDate}
          sx={textFieldSize}
        /> */}
         <LocalizationProvider dateAdapter={AdapterMoment}>
          <TextField
              required
              onChange={(e) => handleNewDueDate(e)}
              label='Due Date'
              type='date'
              variant='standard'
              value={newDueDate}
              sx={textFieldSize}
              InputLabelProps={{
                shrink: true,
              }}
          />
        </LocalizationProvider>
      </Box>

      <Box sx={boxStyle}>
        <Typography sx={titleSize}>

          Created on
        </Typography>
        <TextField
          variant='standard'
          value={researchItem?.createdOn}
          sx={textFieldSize}
        />
      </Box>
      {
      ((researchItem?.status === 'Pending' && researchItem?.assignee === userData?.email) 
        || 
      (researchItem?.status === 'Requested' && researchItem?.creator === userData?.uid))
      &&
       <Button variant='outlined' sx={updateAssignee} onClick={updateNewStatus}>Update as Assignee</Button>}
    </Box>
  )

}

export default InReview
