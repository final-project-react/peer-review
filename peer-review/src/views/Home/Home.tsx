import React, { useContext } from 'react';
import { Typography } from '@mui/material';
import { Box } from '@mui/system';
import LoginSignupCard from '../../components/LoginSignupCard/LoginSignupCard';
import Lottie from 'react-lottie';
import animation from './../../imageSrc/animation/hero-animation.json';
import './home.css';
import Header from '../../components/Header/Header';
import { mainFont } from '../../ui-helpers/ui-helpers';
import { AuthContext } from '../../Context/Context';

const Home = (props: any) => {
  const { user } = useContext(AuthContext);

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animation,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice',
    },
  };

  return (
    <Box>
      <Box>
        <Header />
      </Box>
      <div className='home'>
        <span style={{ width: '60%', height: '100%' }}>
          <Typography
            variant='h3'
            sx={{
              fontFamily: mainFont,
              paddingLeft: '50px',
              textAlign: 'center',
              marginBottom: '38px',
            }}
          >
            We make team managing simple.
          </Typography>
          {/* <img src={logo} alt='home' style={{height: '800px', width: '90%'}}/> */}
          <Lottie options={defaultOptions} height={'80%'} width={'80%'} />
        </span>
        <Box sx={{ width: '30%', paddingRight: '120px' }}>
          <LoginSignupCard />
        </Box>
      </div>
    </Box>
  );
};

export default Home;
