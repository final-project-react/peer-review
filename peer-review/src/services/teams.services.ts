import { push, ref, get, update } from '@firebase/database';
import { db } from '../firebase/firebaseConfig';

export const createTeam = (
  name: string,
  description: string,
  ownerId: string,
  members: {},
  ownerEmail: string
) => {
  return push(ref(db, 'teams'), {
    name,
    description,
    ownerId,
    createdOn: new Date().toLocaleDateString(),
    members,
    workItems: {},
    ownerEmail,
  });
};

export const getAllTeams = () => {
  return get(ref(db, 'teams'));
};

export const getTeamById = (teamId: string) => {
  return get(ref(db, `teams/${teamId}`));
};

export const updateTeamUsers = (teamId: string, uid: string, email: string) => {
  const updateTeam: any = {};
  updateTeam[`teams/${teamId}/members/${uid}`] = email;
  return update(ref(db), updateTeam);
};

export const removeUserFromTeam = (teamId: string, uid: string) => {
  const updateTeam: any = {};
  updateTeam[`teams/${teamId}/members/${uid}`] = null;
  return update(ref(db), updateTeam);
};

export const deleteTeamById = (teamId: string) => {
  const updateTeam: any = {};
  updateTeam[`teams/${teamId}`] = null;
  return update(ref(db), updateTeam);
};
