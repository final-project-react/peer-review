import { get, ref, update } from '@firebase/database';
import { db } from '../firebase/firebaseConfig';

export const removeItemComents = (itemId: string) => {
  const removeComent: any = {};
  removeComent[`comments/${itemId}`] = null;
  return update(ref(db), removeComent);
};


export const getCommentById = (researchItemId : string | undefined | null, commentId: string) => {

  return get(ref(db, `comments/${researchItemId}/${commentId}`))
}