import { updateEmail } from '@firebase/auth';
import {
  equalTo,
  get,
  onValue,
  orderByChild,
  push,
  query,
  ref,
  set,
  update,
} from 'firebase/database';
import { ResearchItem } from '../components/Interfaces/Interfaces';
import { UserData } from '../Context/Context';
import { db } from '../firebase/firebaseConfig';
import { getAllResearches, updateResearchAvatar } from './researchItem';

export const createUserData = (
  firstName: string,
  lastName: string,
  username: string,
  email: string,
  phoneNumber: string,
  uid: string
) => {
  return set(ref(db, `users/${uid}`), {
    firstName,
    lastName,
    username,
    email,
    phoneNumber,
    uid,
    userRole: 'member',
    createdOn: new Date().toLocaleDateString(),
    avatar: '',
    teams: {},
    badges: 0,
  });
};

export const updateUserEmail = (uid: string, newEmail: string) => {
  const updateEmail: any = {};
  updateEmail[`/users/${uid}/email`] = newEmail;
  return update(ref(db), updateEmail);
};

export const updateUserAvatar = (uid: string, imageURL: string) => {
  const updateAvatar: any = {};

  updateAvatar[`/users/${uid}/avatar`] = imageURL;

  return update(ref(db), updateAvatar);
};
export const updateUserBadges = (uid: string, newBadges: string | number) => {
  const updateAvatar: any = {};

  updateAvatar[`/users/${uid}/badges`] = newBadges;

  return update(ref(db), updateAvatar);
};

export const updateAllUserPostAvatar = async (
  uid: string,
  avatarUrl: string
) => {
  const data = await getAllResearches();
  const allResearches: [string, ResearchItem][] = Object.entries(data.val());
  allResearches.forEach((research) => {
    const creatorId: string = research[1].creator;
    const itemId: string = research[0];
    if (creatorId === uid) {
      updateResearchAvatar(itemId, avatarUrl);
    }
  });
};

export const updateUserUsername = (uid: string, newUsername: string) => {
  const updateUsername: any = {};
  updateUsername[`/users/${uid}/username`] = newUsername;
  return update(ref(db), updateUsername);
};
export const getUserData = (uid: string) => {
  return get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)));
};

export const getAllUsers = () => {
  return get(ref(db, 'users'));
};

export const findUserIdByEmail = (email: string | undefined) => {
  return getAllUsers().then((users) => {
    if (users.val()) {
      const userData: UserData[] = users.val();
      const user = Object.values(userData).filter(
        (user) => user.email === email
      );
      return user[0];
    }
  });
};

export const updateUserResearchItem = (
  uid: string | undefined,
  researchId: string | null
) => {
  const updateUser: any = {};
  updateUser[`users/${uid}/researchItems/${researchId}`] = true;

  return update(ref(db), updateUser);
};

export const getUserResearchItems = (uid: string | undefined) => {
  return get(ref(db, `users/${uid}/researchItems`));
};
export const updateUserTeam = (uid: string, teamId: string) => {
  const updateTeam: any = {};
  updateTeam[`/users/${uid}/teams/${teamId}`] = true;
  return update(ref(db), updateTeam);
};

export const updateOwnerUserTeam = (uid: string, teamId: string) => {
  const updateTeam: any = {};
  updateTeam[`users/${uid}/ownTeams/${teamId}`] = true;
  return update(ref(db), updateTeam);
};

export const removeTeamIdFromUser = (uid: string, teamId: string) => {
  const updateTeam: any = {};
  updateTeam[`users/${uid}/teams/${teamId}`] = null;
  return update(ref(db), updateTeam);
};

export const removeOwnerUserTeam = (uid: string, teamId: string) => {
  const updateTeam: any = {};
  updateTeam[`users/${uid}/ownTeams/${teamId}`] = null;
  return update(ref(db), updateTeam);
};

export const createNotification = (userId: string | undefined, data: {}) => {
  return push(ref(db, `notifications/${userId}`), data);
};

export const getAllNotificationByUserId = (userId: string | undefined) => {
  return get(ref(db, `notifications/${userId}`));
};

export const getAllNotificationByUserIdLive = (listen: any) => {
  return onValue(ref(db, `notifications`), listen);
};

export const deleteNotification = (
  userId: string | undefined,
  notificationId: string
) => {
  const deleteNotif: any = {};

  deleteNotif[`notifications/${userId}/${notificationId}`] = null;

  return update(ref(db), deleteNotif);
};
export const updateUserRole = (uid: string, role: string) => {
  const updateRole: any = {};
  updateRole[`users/${uid}/userRole`] = role;
  return update(ref(db), updateRole);
};
