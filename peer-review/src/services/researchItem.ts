import { get, push, ref, update } from '@firebase/database';
import { db } from '../firebase/firebaseConfig';
import { updateUserResearchItem } from './user.services';

export const createResearch = (
  avatar: string | undefined,
  title: string,
  content: {},
  assignee: string,
  assigneeAvatar: string | undefined,
  reporter: string,
  dueDate: string,
  createdOn: string,
  priority: string,
  team: string,
  uid: string | undefined,
  creator: string,
  status: string,
  teamId: string | undefined,
  file: string
) => {
  return push(ref(db, 'researchItems'), {
    avatar,
    title,
    content,
    assignee,
    assigneeAvatar,
    reporter,
    dueDate,
    createdOn,
    priority,
    team,
    creator,
    status,
    teamId,
    file,
  }).then((res) => updateUserResearchItem(uid, res.key));
};

export const getResearchById = (id: string | undefined) => {
  return get(ref(db, `researchItems/${id}`));
};

export const getAllResearches = () => {
  return get(ref(db, 'researchItems'));
};

export const updateResearchAvatar = (itemId: string, avatarUrl: string) => {
  const updatePost: any = {};
  updatePost[`/researchItems/${itemId}/avatar`] = avatarUrl;

  return update(ref(db), updatePost);
};
export const changeItemStatus = (
  itemId: string | undefined,
  newStatus: string
) => {
  const updateStatus: any = {};

  updateStatus[`researchItems/${itemId}/status`] = newStatus;

  return update(ref(db), updateStatus);
};

export const changeItemDueDate = (
  itemId: string | undefined,
  newDueDate: string
) => {
  const updateDueDate: any = {};

  updateDueDate[`researchItems/${itemId}/dueDate`] = newDueDate;

  return update(ref(db), updateDueDate);
};

export const removeFromUserItem = (uid: string, itemId: string) => {
  const updateItem: any = {};
  updateItem[`users/${uid}/researchItems/${itemId}`] = null;
  return update(ref(db), updateItem);
};

export const deleteItem = (itemId: string) => {
  const removeItem: any = {};
  removeItem[`researchItems/${itemId}`] = null;
  return update(ref(db), removeItem);
};
export const changeContentResearchItem = (itemId: string, content: any) => {
  const updateResearchItem: any = {};

  updateResearchItem[`researchItems/${itemId}/content`] = content;

  return update(ref(db), updateResearchItem);
};

export const addComment = (
  itemId: string | undefined | null,
  commentData: {}
) => {
  return push(ref(db, `comments/${itemId}`), commentData);
};

export const getAllCommentsByResearchItemId = (
  itemId: string | null | undefined
) => {
  return get(ref(db, `comments/${itemId}`));
};

export const deleteComment = (
  itemId: string | null | undefined,
  commentId: string
) => {
  const deleteComment: any = {};

  deleteComment[`comments/${itemId}/${commentId}`] = null;

  return update(ref(db), deleteComment);
};

export const oldContent = (itemId: string, content: {}) => {
  const oldDataContent: any = {};
  oldDataContent[`researchItems/${itemId}/oldContent`] = content;
  return update(ref(db), oldDataContent);
};

export const updateResearchAvatarAssignee = (
  itemId: string,
  avatarUrl: string
) => {
  const updatePost: any = {};
  updatePost[`/researchItems/${itemId}/assigneeAvatar`] = avatarUrl;

  return update(ref(db), updatePost);
};
